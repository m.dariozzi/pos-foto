package com.example.posfoto.sessions;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

public class AppGeneralData {

    SharedPreferences preferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "com.example.posfoto.general_app_data";

    private static final String IS_LOGIN = "isLoggedIn";

    private static final String PREF_VERSION_CODE_KEY = "version_code";




    public AppGeneralData(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }




    public void setDate(Date date) {
        preferences.edit().putLong("date_of_last_synchronisation", date.getTime()).apply();
    }

    public java.util.Date getDate(){
        Date date = new Date(preferences.getLong("date_of_last_synchronisation", 0));
        return date;
    }
}
