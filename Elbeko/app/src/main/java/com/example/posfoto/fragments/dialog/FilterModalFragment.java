package com.example.posfoto.fragments.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.example.posfoto.R;
import com.example.posfoto.models.Project;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;


public class FilterModalFragment extends DialogFragment {

    String title;
    private static String PROJECTS_KEY = "projects_key";
    private ArrayList<Project> projects = new ArrayList<>();
    FilterModalFragmentListener list;

    public  FilterModalFragment() {

    }



    public FilterModalFragment(FilterModalFragmentListener list) { this.list = list;}

    public static FilterModalFragment newInstance(String title, ArrayList<Project> projects) {
        FilterModalFragment frag = new FilterModalFragment();
        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putParcelableArrayList(PROJECTS_KEY, projects);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.title = getArguments().getString("Title");
            this.projects = (getArguments().getParcelableArrayList(PROJECTS_KEY));
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
        builder.setNeutralButton(getResources().getString(R.string.cancel), (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });



        ArrayList<String> projectsToDisplay = new ArrayList<>();
        for (int i = 0; i < projects.size(); i++) {
            projectsToDisplay.add(projects.get(i).getProjectName());
        }

        List<String> test = projectsToDisplay.stream().distinct().collect(Collectors.toList());


        builder.setTitle("test");
        boolean[] checkedItems = {false, false};

        builder.setMultiChoiceItems(test.toArray(new String[test.size()]), checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                // set the filters
            }
        });
//
//        String[] animals = {"A-z", "Z-A"};
//        builder.setSingleChoiceItems(animals, 0, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                // user checked an item
//            }
//        });


        builder.setPositiveButton("ok", (dialogInterface, i) -> {
                    //return the filters
//            list.onSelectFilter();
        });

        return builder.create();
    }


    public interface FilterModalFragmentListener {
        void onSelectFilter();
    }
}