package com.example.posfoto.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.posfoto.R;
import com.example.posfoto.adapters.AuthorSpinnerArrayAdapter;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.sessions.SessionManager;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Date;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class EditProjectActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextInputEditText txtProjectName;
    private TextInputEditText txtPosInternalCode;
    private Spinner spnProjectManager;
    private TextInputEditText txtProjectManagerPosInternalCode;

    private ProjectViewModel viewModel;
    private Project edit_project;
    private Author projectManager;

    private HashMap<String, String> user;

    AuthorSpinnerArrayAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtProjectName = findViewById(R.id.txtProjectName);
        txtPosInternalCode = findViewById(R.id.txtPos_internal_code);
        txtProjectManagerPosInternalCode = findViewById(R.id.txtProjectManagerPosInternalCode);
        spnProjectManager = findViewById(R.id.spnSelectProjectManager);

        edit_project = (Project) getIntent().getSerializableExtra("edit_project");
        viewModel = ViewModelProviders.of(this).get(ProjectViewModel.class);

        SessionManager session = new SessionManager(getApplicationContext());
        user = session.getUserDetails();

        adapter = new AuthorSpinnerArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item);
            new ViewModelProvider(this).get(AuthorViewModel.class).getAllAuthors().observe(this, authors -> {
                authors.add(0, new Author(-1, "choose author", "", "", "", "" , Author.Role.USER, new Date(), ""));
                adapter.setAuthors(authors);

                if (edit_project != null) {
                    spnProjectManager.setSelection(authors.indexOf(authors.stream().filter(author -> edit_project.projectManagerId == author.getAuthorId()).findFirst().get()) + 1);
                } else {
                    spnProjectManager.setSelection(authors.indexOf(authors.stream().filter(author ->Integer.parseInt(user.get("userId")) == author.getAuthorId()).findFirst().get()) + 1);
                }
            });


        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnProjectManager.setAdapter(adapter);
        spnProjectManager.setOnItemSelectedListener(this);


        if (edit_project != null) {
            txtProjectName.setText(edit_project.getProjectName());
            txtPosInternalCode.setText(edit_project.getPOSInternalCode());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_annotation_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                getSupportFragmentManager().popBackStackImmediate();
                finish();
                return true;
            case R.id.save_annotation_row:
                try {
                    if (edit_project != null) {
                        edit_project.setProjectId(edit_project.getProjectId());
                        edit_project.setProjectName(txtProjectName.getText().toString());
                        edit_project.setUpdated_at(new Date());
                        viewModel.update(edit_project);
                    } else {
                        //new project

                        Project last_project = (Project) getIntent().getSerializableExtra("current_projects");
                        Project new_project =
                                new Project(last_project.getProjectId() + 1,
                                        txtProjectName.getText().toString(),
                                        txtPosInternalCode.getText().toString(),
                                        new Date(),
                                        Integer.parseInt(user.get("userId")));
                        viewModel.insert(new_project);

                    }
                } catch (Exception e) {
                    Toast.makeText(this, "something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    Log.e("addproject_TAG", "onOptionsItemSelected: ", e);
                    finish();
                }
               //new project

                setResult(Activity.RESULT_OK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        AuthorSpinnerArrayAdapter adapter = (AuthorSpinnerArrayAdapter) parent.getAdapter();
        txtProjectManagerPosInternalCode.setText(adapter.getAuthors().get(position).getPOS_internal_code());

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }
}
