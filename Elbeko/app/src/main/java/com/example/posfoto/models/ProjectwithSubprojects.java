package com.example.posfoto.models;

import java.util.List;

import androidx.room.Embedded;
import androidx.room.Relation;

public class ProjectwithSubprojects {
    @Embedded
    public Project project;
    @Relation(parentColumn = "projectId",
    entityColumn = "linked_project_id"
    )
    public List<Subproject> subprojects;

}
