package com.example.posfoto.network.interfaces;

public interface CheckNetworkInterface {
        public void onAvailable();
        public void onLost();
    }
