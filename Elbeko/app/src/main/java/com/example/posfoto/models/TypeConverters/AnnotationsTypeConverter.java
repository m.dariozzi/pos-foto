package com.example.posfoto.models.TypeConverters;

import java.lang.reflect.Array;
import java.util.ArrayList;

import androidx.room.TypeConverter;


public class AnnotationsTypeConverter {

    @TypeConverter
    public static String fromArray(ArrayList<String> strings) {
        String string = "";
        for(String s : strings) string += (s + ",");

        return string;
    }

    @TypeConverter
    public ArrayList<String> toArray(String concatenatedStrings) {
        ArrayList<String> myStrings = new ArrayList<>();
        for(String s : concatenatedStrings.split(",")) myStrings.add(s);

        return myStrings;
    }
}
