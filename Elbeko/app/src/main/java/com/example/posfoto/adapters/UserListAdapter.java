package com.example.posfoto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.posfoto.R;
import com.example.posfoto.models.Author;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> {

    private Context context;


    class UserViewHolder extends RecyclerView.ViewHolder {
        private final TextView userItemTitle;
        private final TextView userItemText1;
        private final TextView userItemText2;
        private final TextView userItemTextDate;
//        private final Button userItemSendBtn;

        private UserViewHolder(View itemView) {
            super(itemView);

            userItemTitle = itemView.findViewById(R.id.list_item_title);
            userItemText1 = itemView.findViewById(R.id.list_item_text1);
            userItemText2 = itemView.findViewById(R.id.list_item_text2);
            userItemTextDate = itemView.findViewById(R.id.list_item_date);
//            userItemSendBtn = itemView.findViewById(R.id.btnSendSingleAnnotation);
        }

        public void bind(final Author item, final UserListAdapter.OnItemClickListener listener) {
            userItemTitle.setText(item.toString());
            userItemText1.setText(item.getUsername() + " " + item.getRole().toString());
            userItemText2.setText(item.getPOS_internal_code());
            userItemTextDate.setText(item.getFormattedUpdated_at());

//            userItemSendBtn.setVisibility(View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, v);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemLongClick(item, v);

                    return true;
                }
            });
        }
    }


    public interface OnItemClickListener {
        void onItemClick(Author item, View v);
        boolean onItemLongClick(Author item, View v);
    }

    private final LayoutInflater inflater;

    private List<Author> users;

    private final OnItemClickListener listener;

    public UserListAdapter(Context context, OnItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.list_item, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        if (users != null) {
            holder.bind(users.get(position), listener);

        } else {
            holder.userItemTitle.setText("no users");
        }
    }


    public void setAuthors(List<Author> users){
        this.users = users;
        notifyDataSetChanged();
    }

    public List<Author> getAuthors() {
        return this.users;
    }

    @Override
    public int getItemCount() {
        if (users != null) {
            return users.size();
        } else {
            return 0;
        }
    }
}
