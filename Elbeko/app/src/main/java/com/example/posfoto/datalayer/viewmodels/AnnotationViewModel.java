package com.example.posfoto.datalayer.viewmodels;

import android.app.Application;

import com.example.posfoto.datalayer.repositories.AnnotationRepository;
import com.example.posfoto.datalayer.repositories.AuthorRepository;
import com.example.posfoto.datalayer.repositories.ProjectRepository;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.Serverdata;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;

public class AnnotationViewModel extends AndroidViewModel {

    private AnnotationRepository repository;
    private AuthorRepository authorRepository;
    private ProjectRepository projectRepository;

    private LiveData<List<Annotation>> allAnnotations;

    private MutableLiveData<List<Annotation>> annotationsFromServer;

    private LiveData<List<Author>> allAuthors;
    private LiveData<List<Project>> allProjects;


    public AnnotationViewModel(@NonNull Application application) {
        super(application);
        repository = new AnnotationRepository(application);

        authorRepository = new AuthorRepository(application);
        projectRepository = new ProjectRepository(application);

        allAnnotations = repository.getAllAnnotations();
        allAuthors = authorRepository.getAllAuthors();
        allProjects = projectRepository.getAllProjects();
    }

    public LiveData<List<Annotation>> getAllAnnotations() { return allAnnotations; }

    public LiveData<List<Annotation>> getAllAnnotationsById(int userId) { return repository.getAllAnnotationsById(userId); }

    public void insert(Annotation annotation) { repository.insert(annotation);}

    public void deleteById(int id) { repository.deleteById(id);}

    public void update(Annotation annotation) { repository.update(annotation); }

    public LiveData<List<Author>> getAllAuthors() { return allAuthors; }

    public LiveData<List<Project>> getAllProjects() { return allProjects; }

    public LiveData<List<Annotation>> getUnsentAnnotations() { return repository.getUnsentAnnotations(); }

    public LiveData<List<Annotation>> getUnsentAnnotationsByProject(int projectId) { return repository.getUnsentAnnotationsByProject(projectId); }

    public LiveData<List<Annotation>> getSentAnnotations() { return  repository.getSentAnnotations(); }

    public Call<List<Serverdata>> getAnnotationsFromServer() {
        return repository.getAnnotationsFromServer();
    }

    public Call<List<Serverdata>> getAnnotationsByProject_id(String id) {
        return repository.getAnnotationsByProject(id);
    }

    public Call<List<Project>> getAllProjectsFromServer() {
        return repository.getProjectsFromServer();
    }

    public void deleteAll() {
        repository.deleteAll();
    }


}
