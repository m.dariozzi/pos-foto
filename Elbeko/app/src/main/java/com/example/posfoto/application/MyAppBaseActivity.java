package com.example.posfoto.application;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.posfoto.BuildConfig;
import com.example.posfoto.R;
import com.example.posfoto.helper.CheckNetwork;
import com.example.posfoto.network.interfaces.CheckNetworkInterface;
import com.example.posfoto.sessions.SessionManager;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class MyAppBaseActivity extends AppCompatActivity implements CheckNetworkInterface {

    protected View content;
    private SessionManager session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = findViewById(android.R.id.content);

        session = new SessionManager(getApplicationContext());
        session.setSelectedServer(BuildConfig.URL_ARRAY[0]);

        CheckNetwork network = new CheckNetwork(getApplicationContext(), session, this);
        network.registerNetworkCallback(content);

        CreateNotificationChannel();
    }


    private void CreateNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "POS_FOTO_NOT_CHAN";
            String description = "notification channel for pos foto application";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("POS-FOTO", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
    }

    @Override
    public void onAvailable() {
        //if connected, check if toolbar, if toolbar put standard color
        //if connection gained->ask for synchronization -> send session for JWT token.
        Log.d("TAG", "Connected");
        Log.d("TAG", session.getUserDetails().toString());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Stuff that updates the UI
                if (content.findViewById(R.id.homeFABSend) != null) {
                    content.findViewById(R.id.homeFABSend).setEnabled(true);
                    content.findViewById(R.id.homeFABSend).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }

            }
        });



        if (session.getUserDetails().get("bearer_token") == null) {
            //maken new request for token

//            ask for synchronisation
//            Snackbar snack = Snackbar.make(findViewById(android.R.id.content), "Synchronize with the server?", BaseTransientBottomBar.LENGTH_LONG)
//                    .setAction("yes", view -> Log.d("TAG", "pressed"));
//
//            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)
//                    snack.getView().getLayoutParams();
//            params.setMargins(0, 10, 0, 0);
//            snack.getView().setLayoutParams(params);
//            snack.show();
        }


    }

    @Override
    public void onLost() {
        Snackbar.make(content, "Internet connection lost.", BaseTransientBottomBar.LENGTH_LONG).show();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Stuff that updates the UI
                if (content.findViewById(R.id.homeFABSend) != null) {
                    content.findViewById(R.id.homeFABSend).setEnabled(false);
                    content.findViewById(R.id.homeFABSend).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.material_on_surface_disabled)));
                }

            }
        });
    }

}
