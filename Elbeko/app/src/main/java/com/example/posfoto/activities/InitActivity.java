package com.example.posfoto.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.example.posfoto.R;
import com.example.posfoto.adapters.ProjectSpinnerArrayAdapter;
import com.example.posfoto.application.MyAppBaseActivity;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.models.Project;
import com.example.posfoto.sessions.SessionManager;

import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

public class InitActivity extends MyAppBaseActivity {

    private ProjectViewModel prvm;
    Button btnSelectProject;
    Button btnNoProject;
    ProjectSpinnerArrayAdapter projectAdapter;
    Spinner spnProject;
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);

        session = new SessionManager(getApplicationContext());

        btnSelectProject = findViewById(R.id.btnSelectProject);
        btnNoProject = findViewById(R.id.btnNoProject);


        prvm = new ViewModelProvider(this).get(ProjectViewModel.class);
        projectAdapter = new ProjectSpinnerArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item);
        spnProject = findViewById(R.id.spnSelectProjectForLogin);

        spnProject.setEnabled(false);
        spnProject.setVisibility(View.INVISIBLE);
        spnProject.setAdapter(projectAdapter);

        //fill the spinner with data
        prvm.getAllProjects().observe(InitActivity.this, (Observer<List<Project>>) projects -> {

            if (projects.size() != 0) {
                projectAdapter.setProjects(projects);
            } else {
                btnSelectProject.setEnabled(false);
                btnSelectProject.getBackground().setColorFilter(ContextCompat.getColor(InitActivity.this, android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
            }

        });

//        prvm.getProjectWithSubprojects().observe(InitActivity.this, new Observer<List<ProjectwithSubprojects>>() {
//            @Override
//            public void onChanged(List<ProjectwithSubprojects> projectwithSubprojects) {
//                Log.d("TAG", projectwithSubprojects.toString());
//            }
//        });

        spnProject.setVisibility(View.VISIBLE);
        spnProject.setEnabled(true);
        btnSelectProject.setVisibility(View.VISIBLE);

        btnSelectProject.setOnClickListener(view -> {
            session.setSelectedProject(projectAdapter.getProjects().get(spnProject.getSelectedItemPosition()));
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        });

        btnNoProject.setOnClickListener(view -> {
            session.setSelectedProject(null);
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        });


    }
}