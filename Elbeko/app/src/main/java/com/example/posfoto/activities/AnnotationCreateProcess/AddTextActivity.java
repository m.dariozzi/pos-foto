package com.example.posfoto.activities.AnnotationCreateProcess;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.posfoto.R;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.models.Project;
import com.example.posfoto.sessions.SessionManager;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

public class AddTextActivity extends AppCompatActivity {

    EditText mText;
    Button mBtnNext;
    Button mbtnBack;

    SessionManager session;
    AnnotationViewModel annotationViewModel;

    List<Uri> mImageUri = new ArrayList<>();
    Project selectedProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_text);

        mText = findViewById(R.id.add_text_textInput);
        mBtnNext = findViewById(R.id.add_text_next_button);
        mbtnBack = findViewById(R.id.add_text_back_button);

        mImageUri = (List<Uri>) getIntent().getSerializableExtra("pictures");
        selectedProject = getIntent().getParcelableExtra("project");

        session = new SessionManager(getApplicationContext());
        annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);

        mBtnNext.setOnClickListener(view -> {
            Intent addAnnotationParamsIntent = new Intent(this, AddAnnotationParametersActivity.class);
            addAnnotationParamsIntent.putExtra("pictures", (ArrayList) mImageUri);
            addAnnotationParamsIntent.putExtra("annotationText", mText.getText());
            startActivity(addAnnotationParamsIntent);


        });

        mbtnBack.setOnClickListener(view -> {
            this.finish();
        });

    }
}