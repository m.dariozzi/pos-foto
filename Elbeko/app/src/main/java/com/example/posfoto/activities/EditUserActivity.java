package com.example.posfoto.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.posfoto.R;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.example.posfoto.models.Author;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

public class EditUserActivity extends AppCompatActivity {

    private TextInputEditText txtFirstName;
    private TextInputEditText txtLastName;
    private TextInputEditText txtEmail;
    private TextInputEditText txtPosInternalCode;
    private AuthorViewModel viewModel;
    private Author edit_author;

    Author new_author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        txtFirstName = findViewById(R.id.txtUserFirstname);
        txtLastName = findViewById(R.id.txtUserLastname);
        txtEmail = findViewById(R.id.txtUserEmail);
        txtPosInternalCode = findViewById(R.id.txtUserPosInternalCode);

        edit_author = (Author) getIntent().getSerializableExtra("edit_author");

        if (edit_author != null) {
            txtFirstName.setText(edit_author.getFirstName());
            txtLastName.setText(edit_author.getLastName());
            txtEmail.setText(edit_author.getEmail());
            txtPosInternalCode.setText(edit_author.getPOS_internal_code());
        }

        viewModel = new ViewModelProvider(this).get(AuthorViewModel.class);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_annotation_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                getSupportFragmentManager().popBackStackImmediate();
                finish();
                return true;
            case R.id.save_annotation_row:

                try {
                    if (edit_author != null) {
                        //edit author
                        edit_author.setAuthorId(edit_author.getAuthorId());
                        edit_author.setFirstName(txtFirstName.getText().toString());
                        edit_author.setLastName(txtLastName.getText().toString());
                        edit_author.setEmail(txtEmail.getText().toString());
                        edit_author.setPOS_internal_code(txtPosInternalCode.getText().toString());
                        edit_author.setUpdated_at(new Date());
                        viewModel.update(edit_author);
                    } else {
                        Author last_user = (Author) getIntent().getSerializableExtra("current_authors");
                        new_author = new Author(last_user.getAuthorId() + 1,
                                txtFirstName.getText().toString(),
                                txtLastName.getText().toString(),
                                txtEmail.getText().toString(),
                                createUsername(txtFirstName.getText().toString(), txtLastName.getText().toString()),
                                "test",
                                Author.Role.USER,
                                new Date(),
                                txtPosInternalCode.getText().toString());
                        viewModel.insert(new_author);
                    }
                } catch (Exception e) {
                    Toast.makeText(this, "something went wrong, please try again", Toast.LENGTH_SHORT).show();
                    Log.e("addproject_TAG", "onOptionsItemSelected: ", e);
                }
                setResult(Activity.RESULT_OK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String createUsername(String firstname, String lastname) {
        String[] splitLastname;
        splitLastname = lastname.split(" ");
        String usernameString = firstname.substring(0,1);

        for (int i = 0; i < splitLastname.length; i++) {
            usernameString += splitLastname[i].substring(0,1);
        }

        if (splitLastname.length == 0) {
            return firstname.substring(0, 1) + lastname.substring(0, 1);
        } else {
            return usernameString;
        }



    }
}
