package com.example.posfoto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.posfoto.R;
import com.example.posfoto.models.Annotation;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UnsentAnnotationsListAdapter extends RecyclerView.Adapter<UnsentAnnotationsListAdapter.AnnotationViewHolder> {

    private Context context;

    class AnnotationViewHolder extends RecyclerView.ViewHolder {
        private final TextView annotationItemTitle;
        private final TextView annotationItemText1;
        private final TextView annotationItemTextDate;


        public AnnotationViewHolder(@NonNull View itemView) {
            super(itemView);
            annotationItemTitle = itemView.findViewById(R.id.list_item_title);
            annotationItemText1 = itemView.findViewById(R.id.list_item_text1);
            annotationItemTextDate = itemView.findViewById(R.id.list_item_date);
        }

        public void bind(final Annotation item, final UnsentAnnotationsListAdapter.OnItemClickListener listener) {
            annotationItemTitle.setText(String.valueOf(item.getId()) + " " + item.getText());

            if (item.getPictures().size() == 1) {
                annotationItemText1.setText(item.getPictures().size() + " "  + "image");
            } else {
                annotationItemText1.setText(item.getPictures().size() + " " + "images");
            }
            annotationItemTextDate.setText(item.getFormattedCreated_at());


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, v);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemLongClick(item, v);
                    return true;
                }
            });

            

        }
    }

    public interface OnItemClickListener {
        void onItemClick(Annotation item, View v);
        boolean onItemLongClick(Annotation item, View v);
    }

    private final LayoutInflater inflater;
    private List<Annotation> annotations;
    private final UnsentAnnotationsListAdapter.OnItemClickListener listener;

    public UnsentAnnotationsListAdapter(Context context, OnItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }


    @NonNull
    @Override
    public AnnotationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.list_item2, parent, false);
        return new AnnotationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnotationViewHolder holder, int position) {
        if (annotations != null) {
            holder.bind(annotations.get(position), listener);
        }
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
        notifyDataSetChanged();
    }

    public List<Annotation> getAnnotations() {
        if (annotations.size() > 0) {
            return this.annotations;
        } else{
            return null;
        }
    }

    @Override
    public int getItemCount() {
        if (annotations != null) {
            return annotations.size();
        } else {
            return 0;
        }
    }

}


