package com.example.posfoto.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.posfoto.activities.EditUserActivity;
import com.example.posfoto.R;
import com.example.posfoto.adapters.UserListAdapter;
import com.example.posfoto.models.Author;
import com.example.posfoto.sessions.SessionManager;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.List;

public class UsersFragment extends Fragment {

    View view;
    private UserListAdapter adapter;
    private RecyclerView recyclerView;
    private AuthorViewModel authorViewModel;
    private FloatingActionButton ftlBtnAddUsers;
    private ActionMode mActionMode;
    private int EDIT_USER_ACTIVITY_REQUEST_CODE = 3;


    public UsersFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_users, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = view.findViewById(R.id.user_list_view);
        ftlBtnAddUsers = view.findViewById(R.id.FltBtnAddUsers);

        final UserListAdapter adapter = new UserListAdapter(getContext(), new UserListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Author item, View v) {
                Toast.makeText(getContext(), "azeaze", Toast.LENGTH_LONG);
            }

            @Override
            public boolean onItemLongClick(Author item, View v) {
                if (mActionMode != null) {
                    return false;
                }

                switch (item.getRole().toString()) {
                    case "MASTER":
                        Toast.makeText(getContext(), "The master user cannot be altered", Toast.LENGTH_SHORT).show();
                        break;
                    case "USER":
                        mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeUserCallback);
                        mActionMode.setTag(item);
            }
                return true;
            }
        });


        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        authorViewModel = ViewModelProviders.of(this).get(AuthorViewModel.class);
        authorViewModel.getAllAuthors().observe(this, new Observer<List<Author>>() {
            @Override
            public void onChanged(List<Author> authors) {
                adapter.setAuthors(authors);
            }
        });

        SessionManager session = new SessionManager(getContext());
        HashMap<String, String> user = session.getUserDetails();
        switch (user.get("roleCode")) {
            case "MASTER":
                ftlBtnAddUsers.show();
                break;
            case "USER":
                ftlBtnAddUsers.hide();
                break;
            default:
                break;
        }

        ftlBtnAddUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newUserIntent = new Intent(getActivity(), EditUserActivity.class);
                newUserIntent.putExtra("current_authors", adapter.getAuthors().get(adapter.getAuthors().size() - 1));
                startActivity(newUserIntent);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }


    private ActionMode.Callback mActionModeUserCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.annotation_options_menu, menu);
            mode.setTitle("Choose option");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Author author = (Author) mode.getTag();
            switch (item.getItemId()) {
                case R.id.edit_item:
                    Intent editUserIntent = new Intent(getContext(), EditUserActivity.class);
                    editUserIntent.putExtra("edit_author", author);
                    editUserIntent.addCategory(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(editUserIntent, EDIT_USER_ACTIVITY_REQUEST_CODE);
                    mActionMode.finish();
                    return true;
                case R.id.delete_annotation:
                    /*
                    @TODO: eerst proberen sturen naar de server
                    daarna alle annotations ophalen en dan pas de user + annotations verwijderen.
                     */




                    authorViewModel.deleteById(author.getAuthorId());
                    mActionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
}
