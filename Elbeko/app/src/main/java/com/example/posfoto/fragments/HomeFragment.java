package com.example.posfoto.fragments;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.posfoto.R;
import com.example.posfoto.activities.AnnotationActivity;
import com.example.posfoto.activities.AnnotationCreateProcess.PictureSelectActivity;
import com.example.posfoto.activities.LoginActivity;
import com.example.posfoto.adapters.UnsentAnnotationsListAdapter;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.network.NetworkHelper;
import com.example.posfoto.network.interfaces.NetworkEventsInterface;
import com.example.posfoto.sessions.AppGeneralData;
import com.example.posfoto.sessions.SessionManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.content.ContentValues.TAG;

@RuntimePermissions
public class HomeFragment extends Fragment implements NetworkEventsInterface {

    private View view;
    private FloatingActionButton btnSendAll;
    private Button btnNewAnnotation;
    private ConstraintLayout emptyLayout;
    private List<Annotation> current_annotations = new ArrayList<>();


    /**
     * IMPORTANT MAKES THE EVENT HANDLER WORK
     */
    NetworkHelper helper = new NetworkHelper(this);

    private RecyclerView recyclerView;
    private AnnotationViewModel annotationViewModel;
    private SessionManager session;
    private ActionMode mActionMode;

    private AppGeneralData generalData;
    private static final int NEW_ANNOTATION_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDIT_ANNOTATION_ACTIVITY_REQUEST_CODE = 2;

    static NetworkEventsInterface mEvents;



    public HomeFragment() {
        //required empty contructor
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        generalData = new AppGeneralData(getContext());
        session = new SessionManager(getContext());

        btnSendAll = getView().findViewById(R.id.homeFABSend);
        recyclerView = getView().findViewById(R.id.list_unsent_annotations);
        emptyLayout = getView().findViewById(R.id.emptyListView);
        btnNewAnnotation = getView().findViewById(R.id.btnNewAnnotation);


        final UnsentAnnotationsListAdapter adapter = new UnsentAnnotationsListAdapter(getContext(), new UnsentAnnotationsListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Annotation item, View v) {


                ScrollView scrollView = new ScrollView(getContext());
                LinearLayout imageContainer = new LinearLayout(getContext());
                imageContainer.setOrientation(LinearLayout.VERTICAL);

                AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog))
                        .setTitle("annotation nr " + item.getId() + "\n" + "created: " + item.getFormattedCreated_at())
                        .setMessage(item.getText())
//                        .setMessage(item.getProject().getProjectName() + "\n" + item.getText())
                        .setNegativeButton(R.string.close, null);

                for (int i = 0; i < item.getPictures().size(); i++) {
                    ImageView view = new ImageView(getContext());
                    Glide.with(getContext()).load(item.getPictureByIndex(i))
                            .placeholder(R.drawable.ic_image_placeholder).centerCrop().override(500, 500).into(view);
                    imageContainer.addView(view);

                }
                scrollView.addView(imageContainer);
                dialog.setView(scrollView);
                dialog.show();
            }

            @Override
            public boolean onItemLongClick(Annotation item, View v) {
                if (mActionMode != null) {
                    return false;
                }


                mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback);
                mActionMode.setTag(item);
                return true;
            }
        });


        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.notifyDataSetChanged();


        annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);
        LiveData<List<Annotation>> annos = annotationViewModel.getUnsentAnnotations();
        if (session.getSelectedProject() != null) {
            annos = annotationViewModel.getUnsentAnnotationsByProject(session.getSelectedProject().getProjectId());
        } else {
            annos = annotationViewModel.getUnsentAnnotations();
        }

        annos.observe(getViewLifecycleOwner(), annotations -> {

            adapter.setAnnotations(annotations);
            adapter.notifyDataSetChanged();


                    if (adapter.getItemCount() == 0) {
                        recyclerView.setVisibility(View.GONE);
                        emptyLayout.setVisibility(View.VISIBLE);
                        btnSendAll.setVisibility(View.GONE);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                        emptyLayout.setVisibility(View.GONE);
                        btnSendAll.setVisibility(View.VISIBLE);
                    }

            });

        btnSendAll.setOnClickListener(view1 -> {
            btnSendAll.setEnabled(false);
            getView().findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
            launchAnnotationsSender(adapter);
            btnSendAll.setEnabled(true);
        });

        btnNewAnnotation.setOnClickListener(view -> {
            Intent i = new Intent(getContext(), PictureSelectActivity.class);
            i.addCategory(Intent.ACTION_GET_CONTENT);
            startActivityForResult(i, NEW_ANNOTATION_ACTIVITY_REQUEST_CODE);
        });
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    protected void launchAnnotationsSender(UnsentAnnotationsListAdapter adapter) {
        current_annotations = adapter.getAnnotations();

        Thread.UncaughtExceptionHandler h = (th, ex) -> System.out.println("Uncaught exception: " + ex);

        Thread t = new Thread(() -> {
                for (int ChildCount = recyclerView.getChildCount(), i = 0; i < ChildCount; i++) {
                    final RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(recyclerView.getChildAt(i));

                    try {

                        long startTime = System.nanoTime();
                        NetworkHelper.saveAnnotationAndPictures(current_annotations.get(i), getContext(), holder.itemView);
                        long endTime = System.nanoTime();
                        long MethodeDuration = (endTime - startTime);

                        Log.d("TIMING", MethodeDuration + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            t.setUncaughtExceptionHandler(h);
            t.start();

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main_activity, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        HomeFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);

    }


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.annotation_options_menu, menu);
            mode.setTitle("Choose option");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Annotation anno = (Annotation) mode.getTag();

            switch (item.getItemId()) {
                case R.id.edit_item:
                    Intent editAnnotationIntent = new Intent(getContext(), AnnotationActivity.class);
                    editAnnotationIntent.putExtra("edit_annotation", anno);
                    editAnnotationIntent.addCategory(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(editAnnotationIntent, EDIT_ANNOTATION_ACTIVITY_REQUEST_CODE);
                    return true;
                case R.id.delete_annotation:
                    annotationViewModel.deleteById(anno.getId());
                    mActionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    /**
     * Network helper, eventListeners
     * @param item
     */

    @Override
    public void onSuccess(Annotation item, View itemView) {
        item.setStatus(Annotation.Status.SENT);
        itemView.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        //animation
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.left_to_right_slide_out);
        itemView.startAnimation(animation);
        itemView.setVisibility(View.GONE);

        item.status = Annotation.Status.SENT;
        annotationViewModel.update(item);
        current_annotations.clear();
        Toast.makeText(getContext(), "Annotations saved successfully !", Toast.LENGTH_SHORT).show();


        /**
         *
         * Notification
         */
        //create notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), "POS-FOTO")
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle("POS FOTO")
                .setContentText("Annotations sent successfully")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Success"))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);



        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
        notificationManager.notify(0, builder.build());
    }

    @Override
    public void onError(retrofit2.Response<String> response) {
        Log.d(TAG, "onError: " + response.code());
        switch (response.code()) {
            case 401:
                //logout
                //launch login activity
                session.logoutUser();

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                break;

            case 500:

            default:
                Toast.makeText(getContext(), "Someting went wrong", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        btnSendAll = null;
        recyclerView = null;
        emptyLayout = null;
        btnNewAnnotation = null;
    }
}
