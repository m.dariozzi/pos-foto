package com.example.posfoto.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.example.posfoto.R;
import com.example.posfoto.helper.PictureHelper;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Project;
import com.example.posfoto.network.APIs.AnnotationService;
import com.example.posfoto.network.interfaces.NetworkEventsInterface;
import com.example.posfoto.sessions.SessionManager;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NetworkHelper  {

    static NetworkEventsInterface mEvents;
    static SessionManager session;

    public NetworkHelper(NetworkEventsInterface eventsInterface) {
        mEvents = eventsInterface;
    }


    public static void saveAnnotationAndPictures(final Annotation item, final Context context, final View itemView) {
        session = new SessionManager(context);
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(context);
        AnnotationService api = retrofit.create(AnnotationService.class);
        List<File> imagesToSend = new ArrayList<>();
//        List<Bitmap> imagesToPrepare = new ArrayList<>();

        item.setStatus(Annotation.Status.SENDING);
        item.setProject(session.getSelectedProject());
        itemView.findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);


        try{
            for (int i = 0; i < item.getPictures().size(); i++) {

                if (!item.getPictureByIndex(i).equals("")) {
                    if (Build.VERSION.SDK_INT > 27) {



                        ImageDecoder.Source source = ImageDecoder.createSource(context.getContentResolver(), Uri.parse(item.getPictureByIndex(i)));
//                        imagesToPrepare.add(ImageDecoder.decodeBitmap(source));

                        File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "POSFOTO");
                        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                            Log.d("POSFOTO", "failed to create directory");
                        }

                        File file = new File(mediaStorageDir.getPath() + File.separator + "test" + i + ".jpg");

                        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                        ImageDecoder.decodeBitmap(source).compress(Bitmap.CompressFormat.JPEG, 50, os);
                        os.close();

                        imagesToSend.add(file);
                        Log.d("TAG", "UPLOAD: file length = " + file.length());
                        Log.d("TAG", "UPLOAD: file exist = " + file.exists());


                    }
                }
            }


            List<MultipartBody.Part> parts = new ArrayList<>();
            for (int i = 0; i < imagesToSend.size(); i++) {
                parts.add(prepareFilePart(context, "picture" + i, imagesToSend.get(i), item.getPictureByIndex(i)));
            }

            Map<String, Object> partMap = new HashMap<>();
            partMap.put("id", item.getId());
            partMap.put("author", session.getUserAsAuthor());
            partMap.put("project", item.getProject() == null ?
                    new Project(6999, "no project seleted", "6999", item.getCreated_at(), item.getAuthorId()) : item.getProject());
            partMap.put("text", item.getText().equals("") ? "no annotation" : item.getText());
            partMap.put("status", item.status);
            partMap.put("created_at", item.getFormattedCreated_at());

            api.save_picture(parts, partMap).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d("TAG", response.message());
                    if (response.code() == 200) {
                        mEvents.onSuccess(item, itemView);
                    } else {
                        Toast.makeText(context, "Error while sending the annotation, please try again", Toast.LENGTH_SHORT).show();
                        item.status = Annotation.Status.NOT_SENT;
                        itemView.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                        Animation animation = AnimationUtils.loadAnimation(context, R.anim.wiggle);
                        itemView.startAnimation(animation);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    Toast.makeText(context, "Error while sending the annotation, please try again", Toast.LENGTH_SHORT).show();
                    item.status = Annotation.Status.NOT_SENT;
                    itemView.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.wiggle);
                    itemView.startAnimation(animation);
                }
            });

        } catch (FileNotFoundException e) {
            Log.d("filenotfoundexception", "getParams: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("TAG", "savePictureTest: ", e);
            e.printStackTrace();
        }
    }

    public static void getAllProjects() { }

    public static void getAllAuthors() { }


    private static MultipartBody.Part prepareFilePart(Context context, String partName, File file, String imagePath) {
        String extension = context.getContentResolver().getType(Uri.parse(imagePath));
        RequestBody requestFile =
                RequestBody.create(
                        file,
                        MediaType.parse(extension));


        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }


    /**
     * Deprecated annotation save method
     * @param item
     * @param context
     * @param itemView
     */
    public static void sendAnnotation(final Annotation item, final Context context, final View itemView) {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(context);
        AnnotationService api = retrofit.create(AnnotationService.class);

        item.setStatus(Annotation.Status.SENDING);

        List<Bitmap> imagesToSend = new ArrayList<>();
        ArrayList<String> encodedImages = new ArrayList<>();
        ArrayList<String> oldContentUri = new ArrayList<>();
        try {
            for (int i = 0; i < item.getPictures().size(); i++) {

                if (Build.VERSION.SDK_INT > 27) {
                    Uri bmpUri = null;
                    Uri contentUri = Uri.parse(item.getPictureByIndex(i));
                    ImageDecoder.Source source = ImageDecoder.createSource(context.getContentResolver(), contentUri);
                    imagesToSend.add(ImageDecoder.decodeBitmap(source));

                } else {
                    imagesToSend.add(MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(item.getPictureByIndex(i))));
                }
                oldContentUri.add(item.getPictureByIndex(i));
            }
        } catch (FileNotFoundException e) {
            Log.d("filenotfoundexception", "getParams: "+ e.getMessage());
            e.printStackTrace();
            return;


        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if (imagesToSend.size() > 0) {
            for (int h = 0; h < imagesToSend.size(); h++) {
                encodedImages.add("data:image/png;base64," + PictureHelper.ConvertToBase64(imagesToSend.get(h)));
                item.setPictures(encodedImages);
            }
        }



        api.save(item).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                //launch animations
                if (response.code() == 200) {
                    item.setStatus(Annotation.Status.SENT);
                    item.setPictures(oldContentUri);
                    itemView.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    //animation
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.left_to_right_slide_out);
                    itemView.startAnimation(animation);
                    itemView.setVisibility(View.GONE);
//                    mEvents.onSuccess(item);
                } else {
                    Toast.makeText(context, "Error while sending the annotation, please try again", Toast.LENGTH_SHORT).show();
                    item.status = Annotation.Status.NOT_SENT;
                    itemView.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.wiggle);
                    itemView.startAnimation(animation);
                    mEvents.onError(response);

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, "Error while sending the annotation, please try again", Toast.LENGTH_SHORT).show();
                item.status = Annotation.Status.NOT_SENT;
                itemView.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.wiggle);
                itemView.startAnimation(animation);
            }
        });
    }



}
