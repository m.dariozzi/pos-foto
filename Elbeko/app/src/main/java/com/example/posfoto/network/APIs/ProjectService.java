package com.example.posfoto.network.APIs;

import com.example.posfoto.models.Project;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;


public interface ProjectService {

        //get all annotations
        @GET("pos-foto-api/projects")
        Call<List<Project>> getAllProjects();

        //get Single annotation
        @GET("/pos-foto-api/projects/{id}")
        Call<Project> getProjectById(@Path("id") String id);

        @GET("/pos-foto-api/projects/role/{role}")
        Call<List<Project>> getProjectsByRoleofAuthor(@Part String Role);

        @GET("/pos-foto-api/projects/author/{id}")
        Call<List<Project>> getProjectByAuthor(@Path("id") String id);


        @POST("/save/data")
        Call<String> save(@Body RequestBody body);

}
