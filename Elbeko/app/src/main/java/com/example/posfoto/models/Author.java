package com.example.posfoto.models;


import com.example.posfoto.models.TypeConverters.RoleTypeConverter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "authors")
@TypeConverters(RoleTypeConverter.class)
public class Author implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int authorId;


    @ColumnInfo(name = "firstname")
    private String firstName;

    @ColumnInfo(name = "lastname")
    private String lastName;

    @ColumnInfo(name = "email")
    private String email;

    private String username;

    private String password;

    private Role role;

    @ColumnInfo(name = "author_updated_at")
    private Date updated_at;

    @ColumnInfo(name = "author_POS_internal_code")
    private String POS_internal_code;

    public enum Role {
        USER(5),
        MASTER(10);

        private int code;

        Role(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }


    public Author(int authorId, String firstName, String lastName, String email, String username, String password, Role role, Date updated_at, String POS_internal_code) {
        this.authorId = authorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
        this.updated_at = updated_at;
        this.POS_internal_code = POS_internal_code;
    }

    public int getAuthorId() { return authorId; }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() { return this.firstName + " " + this.lastName; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password;  }

    public void setPassword(String password) { this.password = password; }

    public Role getRole() { return role; }

    public Date getUpdated_at() { return updated_at; }

    public void setUpdated_at(Date updated_at) { this.updated_at = updated_at; }

    public String getFormattedUpdated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss", Locale.getDefault());
        return dateFormat.format(updated_at);
    }

    public String getPOS_internal_code() { return POS_internal_code; }

    public void setPOS_internal_code(String POS_internal_code) { this.POS_internal_code = POS_internal_code; }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

}

