package com.example.posfoto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.posfoto.R;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.models.Project;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ProjectSpinnerArrayAdapter extends ArrayAdapter {
        private Context context;
        private AnnotationViewModel annotationViewModel;

        private final LayoutInflater inflater;
        private List<Project> projects;

        public ProjectSpinnerArrayAdapter(@NonNull Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            this.context = context;
            inflater = LayoutInflater.from(context);
        }

//        @Override
//        public boolean isEnabled(int position) {
////            if (position == 0) {
////                return  false;
////            } else {
////                return true;
////            }
//        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);


            TextView tv = (TextView) view;
            if (projects != null) {
                Project current = projects.get(position);
                tv.setText(current.toString());
            } else {
                tv.setText("Loading...");
            }
            return view;
        }

        public void setProjects(List<Project> projects) {
            this.projects = projects;
            notifyDataSetChanged();

        }

        public List<Project> getProjects() {
            return this.projects;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            View view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
            TextView tv = (TextView) view;

            if (projects != null) {
                Project current = projects.get(position);
                tv.setText(current.toString());
            } else {
                tv.setText("Loading...");
            }


            return view;
        }


        public int getCount() {
            if (projects != null) {
                return projects.size();
            } else {
                return 0;
            }
        }
    }
