package com.example.posfoto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.posfoto.BuildConfig;
import com.example.posfoto.R;
import com.example.posfoto.activities.AnnotationCreateProcess.PictureSelectActivity;
import com.example.posfoto.application.MyAppBaseActivity;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.fragments.AnnotationsFragment;
import com.example.posfoto.fragments.HomeFragment;
import com.example.posfoto.fragments.ProjectsFragment;
import com.example.posfoto.fragments.SettingsFragment;
import com.example.posfoto.fragments.UsersFragment;
import com.example.posfoto.fragments.dialog.ProjectSelectDialogFragment;
import com.example.posfoto.helper.GlobalVariables;
import com.example.posfoto.models.Project;
import com.example.posfoto.sessions.AppGeneralData;
import com.example.posfoto.sessions.SessionManager;
import com.google.android.material.navigation.NavigationView;

import java.util.Date;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;


/**
 * @TODO
 * 1) bij het versturen start de animatie niet
 * 2) het duurt te lang
 * 3) een stop knop voorzien
 * 4) annotaties opslaan op de lokale DB als ze niet verstuurd zijn.
 */


public class MainActivity extends MyAppBaseActivity implements ProjectSelectDialogFragment.ProjectSelectDialogListener {

    private SessionManager session;


    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;

    TextView txtCurrentProject;

    public static final int NEW_ANNOTATION_ACTIVITY_REQUEST_CODE = 1;

    private FragmentTransaction ft;

    private AnnotationViewModel annotationViewModel;
    private AuthorViewModel authorViewModel;
    private ProjectViewModel projectViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_main);


        } catch (Exception e) {
            Log.e("TAG", "onCreate: ", e);
            throw e;
        }

        session = new SessionManager(getApplicationContext());


        //as defined in build.gradle file, is temporary solution
        session.setSelectedServer(BuildConfig.URL_ARRAY[0]);


        AppGeneralData appdata = new AppGeneralData(getApplicationContext());
        appdata.setDate(new Date());

        drawerLayout = findViewById(R.id.drawer);

        final Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitle("POS FOTO");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);
        authorViewModel = new ViewModelProvider(this).get(AuthorViewModel.class);
        projectViewModel = new ViewModelProvider(this).get(ProjectViewModel.class);

        navigationView = findViewById(R.id.navigationView);
        navigationView.bringToFront();

        /**
         * @TODO menu header vullen met informatie van de ingelogde gebruiker
         * */
        View headerView = navigationView.getHeaderView(0);
        txtCurrentProject = headerView.findViewById(R.id.txtSideBarHeaderSelectedProject);
        TextView txtConnectionStatus = headerView.findViewById(R.id.txtSidebarHeaderServerConnection);
        TextView txtSidebarHeaderUser = headerView.findViewById(R.id.txtSidebaHeaderUser);

        if (GlobalVariables.isNetworkConnected) {
            txtConnectionStatus.setText(session.getSelectedServer());
        } else {
            txtConnectionStatus.setText("connected locally");
        }

        if (session.getSelectedProject() != null) {
            txtCurrentProject.setText("current project: " + session.getSelectedProject().getProjectName());

        } else {
            txtCurrentProject.setText("no project selected");
        }

        if (session.getUserAsAuthor() != null) {
            txtSidebarHeaderUser.setText("Current user: " + session.getUserAsAuthor().getUsername());
        } else {
            txtSidebarHeaderUser.setText("not logged in");
            session.logoutUser();
        }

        headerView.findViewById(R.id.btnSwitchProject).setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), InitActivity.class);
            startActivity(intent);
        });


        navigationView.setNavigationItemSelectedListener(menuItem -> {
            if (menuItem.isChecked()) {
                menuItem.setChecked(false);
            } else {
                menuItem.setChecked(true);
            }

            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.menu_home:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.your_placeholder, new HomeFragment());
                    ft.addToBackStack("home");
                    ft.commit();

                    break;
                case R.id.menu_annotations:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.your_placeholder, new AnnotationsFragment());
                    ft.addToBackStack("annotations");
                    ft.commit();

                    break;
                case R.id.menu_users:

                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.your_placeholder, new UsersFragment());
                    ft.addToBackStack("users");
                    ft.commit();

                    break;
                case R.id.menu_projects:

                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.your_placeholder, new ProjectsFragment());
                    ft.addToBackStack("projects");
                    ft.commit();

                    break;
                case R.id.menu_logout:
                    session.logoutUser();
                    Intent redirectToLoginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(redirectToLoginIntent);
                    finish();
                    return true;

                case R.id.menu_options:
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.your_placeholder, new SettingsFragment());
                    ft.addToBackStack("settings");
                    ft.commit();
                    break;

                case R.id.menu_communication:
                    Intent goToControlPanelIntent = new Intent(getApplicationContext(), CommunicationActivity.class);
                    startActivity(goToControlPanelIntent);
                    finish();
                default:
                    return true;
            }
            return true;
        });


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.your_placeholder, new HomeFragment());
        ft.commit();
    }


//    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.add_row:
                Intent i = new Intent(this, PictureSelectActivity.class);
                i.addCategory(Intent.ACTION_GET_CONTENT);
                startActivityForResult(i, NEW_ANNOTATION_ACTIVITY_REQUEST_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFinishedSelect(Project project) {
        txtCurrentProject.setText("current project: " + session.getSelectedProject().getProjectName());
    }
}
