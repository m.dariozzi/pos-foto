package com.example.posfoto.network;

import com.example.posfoto.BuildConfig;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    private static final String[] URL_ARRAY = BuildConfig.URL_ARRAY;
    private static final String BASE_URL = URL_ARRAY[0];

    static Gson gson = new GsonBuilder()
            .setLenient()
            .setDateFormat("dd/MM/yy HH:mm:ss")
            .create();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson));

    private static Retrofit retrofit = builder.build();

    private static HttpLoggingInterceptor loggingInterceptor =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.HEADERS)
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();



    public static <S> S createService(Class<S> serviceClass) {
        if (!httpClient.interceptors().contains(loggingInterceptor)) {
            httpClient.addInterceptor(loggingInterceptor);
            httpClient.addNetworkInterceptor(new StethoInterceptor());
            httpClient.retryOnConnectionFailure(true);

            httpClient.addInterceptor(chain -> {
                Request request  = chain
                        .request().newBuilder()
                        .addHeader("Connection", "close")
                        .addHeader("Authorization", "Bearer " + "token")
                        .build();
                return chain.proceed(request);
            });


            builder.client(httpClient.build());
        }

        return retrofit.create(serviceClass);
    }

}
