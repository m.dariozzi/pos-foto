package com.example.posfoto.network;

import android.content.Context;

import com.example.posfoto.sessions.SessionManager;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static SessionManager session;
    private static String BASE_URL;
    private static HostSelectionInterceptor hostInterceptor = new HostSelectionInterceptor();



    static Gson gson = new GsonBuilder()
            .setLenient()
            .setDateFormat("dd/MM/yy HH:mm:ss")
            .create();

    public static Retrofit getRetrofitInstance(Context context) {
        session = new SessionManager(context);
        BASE_URL = session.getSelectedServer();

        if (retrofit == null){


            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.level(HttpLoggingInterceptor.Level.HEADERS);
            loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);


            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addNetworkInterceptor(new StethoInterceptor());
            httpClient.authenticator(new TokenAuthenticator(session));

            httpClient.addInterceptor(loggingInterceptor);
            httpClient.retryOnConnectionFailure(true);


//            httpClient.addInterceptor(hostInterceptor);

            httpClient.addInterceptor(chain -> {
                Request request = chain.request().newBuilder()
                        .addHeader("Connection", "close")
                        //the bearer token
                        .addHeader("Authorization", "Bearer " + session.getUserDetails().get("bearer_token"))
                        .build();
                return chain.proceed(request);
            });


            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    public static HostSelectionInterceptor getHostInterceptor() {
        return hostInterceptor;
    }

}
