package com.example.posfoto.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.posfoto.helper.DateConverterHelper;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Nonnull;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "projects", foreignKeys = {
        @ForeignKey(entity = Author.class, parentColumns = "authorId", childColumns = "project_manager_id")
    }
)
public class Project implements Serializable, Parcelable {

    public static final Parcelable.Creator CREATOR = new  Parcelable.Creator() {
        public Project createFromParcel(Parcel in){
            return new Project(in);
        }

        @Override
        public Project[] newArray(int i) {
            return new Project[i];
        }

    };

    @PrimaryKey(autoGenerate = true)
    @Nonnull
    private int projectId;

    @ColumnInfo(name = "project_name")
    private String projectName;

    @ColumnInfo(name = "project_updated_at")
    private Date updated_at;

    @ColumnInfo(name = "project_POS_internal_code")
    private String POSInternalCode;

    @ColumnInfo(name = "project_manager_id")
    public int projectManagerId;

    public Project(int projectId, String projectName, String POSInternalCode, Date updated_at, int projectManagerId) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.updated_at = updated_at;
        this.POSInternalCode = POSInternalCode;
        this.projectManagerId = projectManagerId;
    }

    public String getProjectName() { return projectName; }

    public void setProjectName(String projectName) { this.projectName = projectName; }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public Date getUpdated_at() { return updated_at; }

    public void setUpdated_at(Date updated_at) { this.updated_at = updated_at; }

    public String getFormattedUpdated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.getDefault());
        return dateFormat.format(updated_at);
    }

    public String getPOSInternalCode() {
        return POSInternalCode;
    }

    public void setPOSInternalCode(String POSInternalCode) { this.POSInternalCode = POSInternalCode; }

    public int getProjectManagerId() {
        return projectManagerId;
    }

    public void setProjectManagerId(int projectManagerId) { this.projectManagerId = projectManagerId; }

    @Override
    public String toString() {
        return projectName;
    }

    /**
     * Parcelling part
     * @return
     */

    public Project(Parcel in){
        this.projectId = in.readInt();
        this.projectName = in.readString();
        this.updated_at = DateConverterHelper.stringToDate(in.readString());
                this.POSInternalCode = in.readString();
        this.projectManagerId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.projectId);
        parcel.writeString(this.projectName);
        parcel.writeString(this.getFormattedUpdated_at());
        parcel.writeString(this.POSInternalCode);
        parcel.writeInt(this.projectManagerId);
    }
}
