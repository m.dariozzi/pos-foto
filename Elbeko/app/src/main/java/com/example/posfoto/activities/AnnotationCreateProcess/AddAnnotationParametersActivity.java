package com.example.posfoto.activities.AnnotationCreateProcess;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.posfoto.R;
import com.example.posfoto.activities.MainActivity;
import com.example.posfoto.adapters.ProjectSpinnerArrayAdapter;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.fragments.dialog.ProjectSelectDialogFragment;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.sessions.SessionManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

public class AddAnnotationParametersActivity extends AppCompatActivity implements  AdapterView.OnItemSelectedListener, ProjectSelectDialogFragment.ProjectSelectDialogListener {

    List<Uri> mImageUri;

    Button mNextButton;
    Button mPrevButton;

    TextView currentAuthor;

    Spinner spnSelectProject;

    SessionManager session;

    ProjectSpinnerArrayAdapter projectAdapter;

    AnnotationViewModel annotationViewModel;

    final Date current_date = new Date();

    String mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_annotation_paramter);
        mImageUri = (List<Uri>) getIntent().getSerializableExtra("pictures");
        mText = getIntent().getExtras().get("annotationText").toString();


        currentAuthor = findViewById(R.id.txtCurrentAuthor);


        mNextButton = findViewById(R.id.add_params_next_button);

        mNextButton.setOnClickListener(view -> {
            saveAnnotation();
        });

        mPrevButton = findViewById(R.id.add_params_back_button);
        mPrevButton.setOnClickListener(View -> this.finish());

        spnSelectProject = findViewById(R.id.spnSelectProject);

        projectAdapter = new ProjectSpinnerArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item);
        session = new SessionManager(getApplicationContext());

        annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);
        ProjectViewModel projectViewModel = new ViewModelProvider(this).get(ProjectViewModel.class);

//        HashMap<String, String> user = session.getUserDetails();
        Author user = session.getUserAsAuthor();
        if (user == null) {
            session.logoutUser();
        }

        Log.d("TAG", "test");
        if (user != null) {
            currentAuthor.setText(session.getUserAsAuthor().getUsername());
        }


        List<Project> projectsToAdd = new ArrayList<>();
        projectsToAdd.add(session.getSelectedProject());
        if (session.getSelectedProject() != null) {
            projectAdapter.setProjects(projectsToAdd);
        } else {
            projectViewModel.getAllProjects().observe(this, projectList -> {
                projectsToAdd.clear();
                projectsToAdd.addAll(projectList);
                FragmentManager fm = getSupportFragmentManager();
                ProjectSelectDialogFragment projectSelectDialogFragment = ProjectSelectDialogFragment.newInstance(getResources().getString(R.string.elect_project_dialog_title),
                        (ArrayList) projectsToAdd, session.getSelectedProject());
                projectSelectDialogFragment.ProjectSelectDialogListener(AddAnnotationParametersActivity.this);
                projectSelectDialogFragment.show(fm, "fragment_edit_name");

            });

        }

        projectAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnSelectProject.setAdapter(projectAdapter);
        spnSelectProject.setOnItemSelectedListener(this);
    }

    public void saveAnnotation() {
        Date current_date = new Date();


        try {
            ArrayList<String> stringUri = new ArrayList<>();

            for (int i = 0; i < mImageUri.size(); i++) {
                stringUri.add(mImageUri.get(i).toString());
            }

            Annotation newAnnotation = new Annotation(
                    session.getUserAsAuthor(),
                    session.getSelectedProject(),
                    stringUri,
                    mText,
                    session.getUserAsAuthor().getAuthorId(),
                    session.getSelectedProject().getProjectId(),
                    current_date
            );
            annotationViewModel.insert(newAnnotation);

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } catch (Exception e) {
            finish();

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onFinishedSelect(Project project) {
        session.setSelectedProject(project);
        List<Project> projectsToAdd = new ArrayList<>();
        projectsToAdd.add(session.getSelectedProject());
        projectAdapter.setProjects(projectsToAdd);
        projectAdapter.notifyDataSetChanged();
    }
}
