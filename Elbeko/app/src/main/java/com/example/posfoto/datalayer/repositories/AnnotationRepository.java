package com.example.posfoto.datalayer.repositories;

import android.app.Application;
import android.os.AsyncTask;

import com.example.posfoto.datalayer.AnnotationRoomDatabase;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.DAOs.AnnotationDao;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.Serverdata;
import com.example.posfoto.network.APIs.AnnotationService;
import com.example.posfoto.network.APIs.ProjectService;
import com.example.posfoto.network.RetrofitClientInstance;

import java.util.List;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Retrofit;

public class AnnotationRepository {
    private AnnotationDao annotationDao;

    private LiveData<List<Annotation>> allAnnotations;
    Serverdata data;

    Retrofit retrofit;

    public AnnotationRepository(Application application) {
        AnnotationRoomDatabase db = AnnotationRoomDatabase.getDatabase(application);
        retrofit = RetrofitClientInstance.getRetrofitInstance(application);



        annotationDao = db.annotationDao();
        allAnnotations = annotationDao.getAllAnnotations();
    }

    public LiveData<List<Annotation>> getAllAnnotations() {
        return allAnnotations;
    }

    public LiveData<List<Annotation>> getAllAnnotationsById(int userId) { return annotationDao.getAllAnnotationsById(userId); }

    public void insert(Annotation annotation) { new insertAsyncTask(annotationDao).execute(annotation); }

    public void deleteById(int id) {
        new deleteAsyncTask(annotationDao).execute(id);
    }

    public void update(Annotation annotation) { new updateAsyncTask(annotationDao).execute(annotation); }

    public LiveData<List<Annotation>> getUnsentAnnotations() { return annotationDao.getUnsentAnnotations(); }

    public LiveData<List<Annotation>> getUnsentAnnotationsByProject(int projectId) { return  annotationDao.getUnsentAnnotationsByProject(projectId); }

    public LiveData<List<Annotation>> getSentAnnotations() { return annotationDao.getSentAnnotations(); }

    public void deleteAll() {
        new deleteAllsyncTask(annotationDao).execute();
    }

    private static class deleteAllsyncTask extends AsyncTask<Void, Void, Void> {
        private AnnotationDao AsyncTaskDao;

        deleteAllsyncTask(AnnotationDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... params) {
            AsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Annotation, Void, Void> {
        private AnnotationDao AsyncTaskDao;

        updateAsyncTask(AnnotationDao dao) { AsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(Annotation... params) {
            AsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private AnnotationDao AsyncTaskDao;

        deleteAsyncTask(AnnotationDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            AsyncTaskDao.deleteById(params[0]);
            return null;
        }
    }

    private static class insertAsyncTask extends AsyncTask<Annotation, Void, Void> {

        private AnnotationDao AsyncTaskDao;

        insertAsyncTask(AnnotationDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Annotation... params) {
            AsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public Call<List<Serverdata>> getAnnotationsFromServer() {
        AnnotationService api = retrofit.create(AnnotationService.class);
        return api.getAllAnnotations();

    }


    public Call<List<Project>> getProjectsFromServer() {
        ProjectService api = retrofit.create(ProjectService.class);
        return api.getAllProjects();
    }

    public Call<List<Serverdata>> getAnnotationsByProject(String id) {
        AnnotationService api = retrofit.create(AnnotationService.class);
        return api.getAnnotationsForProject(id);
    }
}
