package com.example.posfoto.models.DAOs;

import com.example.posfoto.models.Author;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface AuthorDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Author author);

    @Insert
    void insertAll(List<Author> authors);

    @Query("DELETE FROM authors")
    int deleteAll();

    @Update
    void update(Author author);

    @Query("DELETE FROM authors WHERE authorId = :id")
    void deleteById(int id);

    @Query("SELECT * from authors")
    LiveData<List<Author>> getAllAuthors();

    @Query("SELECT * FROM authors")
    List<Author> staticGetAllAuthors();

    @Query("SELECT * FROM authors where authorId = :id")
    LiveData<List<Author>> GetAllAuthorsById(int id);

    @Query("SELECT * from authors WHERE username = :username AND password = :password")
    LiveData<Author> getAuthorByLoginData(String username, String password);

    @Query("DELETE FROM authors WHERE authorId NOT IN(:lstIdAuthor)")
    void deleteOld(List<Integer> lstIdAuthor);
}
