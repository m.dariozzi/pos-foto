package com.example.posfoto.sessions;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.example.posfoto.activities.LoginActivity;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Bearertoken;
import com.example.posfoto.models.Project;
import com.google.gson.Gson;

import java.util.Date;
import java.util.HashMap;

public class SessionManager {

    SharedPreferences preferences;

    Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "UserSession";

    private static final String IS_LOGIN = "isLoggedIn";

    public static final String KEY_USERID = "userId";

    public static final String KEY_EMAIL = "email";

    public static final String KEY_USERNAME = "username";

    public static final String KEY_PASSWORD = "password";

    public static final String KEY_ROLE = "roleCode";

    public static final String KEY_BEARER_TOKEN = "bearer_token";

    public  static final String POS_INTERNAL_CODE = "pos_internal_code";

    public static final String SELECTED_PROJECT = "selected_project";

    public static final String SELECTED_SERVER = "selected_server";
;

    public SessionManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }


    public void createLoginSession(String userId, String email, String username, String password ,String role, String token, String pos_internal_code){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERID, userId);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_PASSWORD, password);
        editor.putString(KEY_ROLE, role);
        editor.putString(KEY_BEARER_TOKEN, token);
        editor.putString(POS_INTERNAL_CODE, pos_internal_code);
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put("userId", preferences.getString(KEY_USERID, null));
        user.put("email", preferences.getString(KEY_EMAIL, null));
        user.put("username", preferences.getString(KEY_USERNAME, null));
        user.put("password", preferences.getString(KEY_PASSWORD, null));
        user.put("roleCode", preferences.getString(KEY_ROLE, null));
        user.put("bearer_token", preferences.getString(KEY_BEARER_TOKEN, null));
        user.put("pos_internal_code", preferences.getString(POS_INTERNAL_CODE, null));
        return user;
    }

    public Author getUserAsAuthor() {
        HashMap<String, String> user = getUserDetails();
        return new Author(
                Integer.parseInt(user.get("userId")),
                "", "",
                (String) user.get("email"),
                (String) user.get("username"),
                user.get("password"),
                Author.Role.USER,
                new Date(),
                (String) user.get("pos_internal_code"));
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }


    public void setSelectedProject(Project project) {
        Gson gson = new Gson();
        String json = gson.toJson(project);
        editor.putString("selected_project", json);
        editor.commit();
    }

    public Project getSelectedProject() {
        Gson gson = new Gson();
        String json = preferences.getString("selected_project", "");
        return (Project) gson.fromJson(json, Project.class);
    }

    public void setSelectedServer(String server) {
        editor.putString(SELECTED_SERVER, server);
        editor.commit();
    }
    public String getSelectedServer() {
        String server = preferences.getString(SELECTED_SERVER, "");
        return server;
    }

    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);
        }

    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void setBearerToken(Bearertoken token) {
        editor.putString("bearer_token", token.getAccessToken());
    }
}