package com.example.posfoto.datalayer.viewmodels;

import android.app.Application;

import com.example.posfoto.datalayer.repositories.AuthorRepository;
import com.example.posfoto.models.Author;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import retrofit2.Call;

public class AuthorViewModel extends AndroidViewModel {

    private AuthorRepository repository;

    public AuthorViewModel(@NonNull Application application) {
        super(application);
        repository = new AuthorRepository(application);
    }

    public LiveData<Author> getAuthorByLoginData(String username, String password) {  return  repository.getAuthorByLoginData(username, password); }

    public LiveData<List<Author>> getAllAuthors() { return repository.getAllAuthors(); }

    public void deleteById(int id) { repository.deleteById(id);}

    public void insert(Author author) { repository.insert(author); }

    public void update(Author author) { repository.update(author); }

    public LiveData<List<Author>> getAllAuthorsById(int id) { return repository.getAllAuthorsById(id); }

    public Call<List<Author>> getAuthorsFromServer() {
        return repository.getAuthorsFromServer();
    }

    public void clearTable() { repository.clearTable(); }

    public void deleteOldAuthors(List<Integer> lstIdAuthor) {
        repository.deleteOldAuthors(lstIdAuthor);
    }

//    public Call<Author> login(Logindata data) { return repository.login(data); }

}
