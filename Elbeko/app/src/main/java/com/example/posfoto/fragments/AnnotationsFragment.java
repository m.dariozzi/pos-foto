package com.example.posfoto.fragments;


import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.posfoto.R;
import com.example.posfoto.adapters.FragmentPageAdapter;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


public class AnnotationsFragment extends Fragment {

    View view;
    private ActionMode mActionMode;


   public AnnotationsFragment() {
    // Required empty public constructor
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Transition transition = TransitionInflater.from(getContext()).inflateTransition(R.transition.slide_left);
        this.getActivity().getWindow().setExitTransition(transition);
        view = inflater.inflate(R.layout.fragment_annotations, container, false);


        ViewPager viewPager =  view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new FragmentPageAdapter(getParentFragmentManager()));

        TabLayout tabLayout = view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        setHasOptionsMenu(true);
        return view;
    }

}
