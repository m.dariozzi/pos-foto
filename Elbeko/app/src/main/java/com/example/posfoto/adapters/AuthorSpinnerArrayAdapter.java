package com.example.posfoto.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.posfoto.R;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AuthorSpinnerArrayAdapter extends ArrayAdapter {
    private Context context;
    private AnnotationViewModel annotationViewModel;

    private final LayoutInflater inflater;
    private List<Author> authors;
    private List<Project> projects;

    public AuthorSpinnerArrayAdapter(@NonNull Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public boolean isEnabled(int position) {
        if (position == 0) {
            return  false;
        } else {
            return true;
        }
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);

        TextView tv = (TextView) view;
        if (authors != null) {
            Author current = authors.get(position);
            tv.setText(current.toString());
        } else {
            tv.setText("Loading...");
        }
        return view;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
        notifyDataSetChanged();
    }

    public List<Author> getAuthors() {
        return this.authors;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
        TextView tv = (TextView) view;

        if (authors != null) {
            Author current = authors.get(position);
            tv.setText(current.toString());
        } else {
            tv.setText("Loading...");
        }

        if (position == 0) {
            tv.setTextColor(Color.GRAY);
        } else {
            tv.setTextColor(Color.BLACK);
        }

        return view;
    }

    public int getCount() {
        if (authors != null) {
            return authors.size();
        } else {
            return 0;
        }
    }
}

