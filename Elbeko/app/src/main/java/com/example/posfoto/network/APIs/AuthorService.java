package com.example.posfoto.network.APIs;

import com.example.posfoto.models.Author;
import com.example.posfoto.models.Bearertoken;
import com.example.posfoto.models.Logindata;
import com.example.posfoto.models.User;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface AuthorService {

    //get all annotations
    @GET("pos-foto-api/authors")
    Call<List<Author>> getAllAuthors();

    //get Single author
    @GET("/pos-foto/authors/{id}")
    Call<Author> getAuthorById(@Path("id") String id);

    ///LOGIN ROUTES


    @POST("/pos-foto-api/author/save")
    Call<String> save(@Body RequestBody body);

    @POST("/pos-foto-api/auth/login")
    Call<User> login(@Body Logindata data);

    @POST("/pos-foto-api/api/tokens")
    Call<Bearertoken> getToken(@Body Logindata data);
}
