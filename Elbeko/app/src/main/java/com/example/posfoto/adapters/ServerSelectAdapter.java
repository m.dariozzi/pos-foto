package com.example.posfoto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.posfoto.R;

import java.util.List;

import javax.annotation.Nonnull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ServerSelectAdapter extends ArrayAdapter {

    private Context context;

    private final LayoutInflater inflater;
    private List<String> serverUrls;

    public ServerSelectAdapter(@Nonnull Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);

        TextView tx = (TextView) view;
        if (serverUrls != null) {
            String url = serverUrls.get(position);
            tx.setText(url);
        } else {
            tx.setText("Loading...");
        }
        return view;
    }

    public void setUrls(List<String> serverUrls) {
        this.serverUrls = serverUrls;
        notifyDataSetChanged();
    }

    public List<String> getServerUrls() { return this.serverUrls; }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);

        TextView tx = (TextView) view;
        if (serverUrls != null) {
            String url = serverUrls.get(position);
            tx.setText(url);
        } else {
            tx.setText("Loading...");
        }
        return view;
    }

    @Override
    public int getCount() {
        if (serverUrls != null) {
            return serverUrls.size();
        } else {
            return 0;
        }
    }
}
