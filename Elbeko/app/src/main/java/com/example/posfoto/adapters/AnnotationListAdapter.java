package com.example.posfoto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.posfoto.R;
import com.example.posfoto.models.Annotation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class AnnotationListAdapter extends RecyclerView.Adapter<AnnotationListAdapter.AnnotationViewHolder> {

    private Context context;

    class AnnotationViewHolder extends RecyclerView.ViewHolder {
        private final TextView annotationItemTitle;
        private final TextView annotationItemText1;
        private final TextView annotationItemText2;
        private final TextView annotationItemText3;

        private final ImageView annotation_list_status_icon;
//        private final Button btnSendSingleAnnotation;


        private AnnotationViewHolder(View itemView){
            super(itemView);
            annotationItemTitle = itemView.findViewById(R.id.list_item_title);
            annotationItemText1 = itemView.findViewById(R.id.list_item_text1);
            annotationItemText2 = itemView.findViewById(R.id.list_item_text2);
            annotationItemText3 = itemView.findViewById(R.id.list_item_text3);

            annotation_list_status_icon = itemView.findViewById(R.id.list_status_icon);

        }


        public void bind(final Annotation item, final OnItemClickListener listener) {


                if (item.status == Annotation.Status.SENDING) {
                    Glide.with(context).load(R.drawable.ic_annotation_status_not_sent).into(annotation_list_status_icon);
                } else if (item.status == Annotation.Status.SENT){
                    Glide.with(context).load(R.drawable.ic_annotation_status_sent).into(annotation_list_status_icon);
                } else {

                }

                annotationItemTitle.setText("Annotation Nr: " + (item.getId() != 0 ? item.getId() : Math.floor(Math.random() * 10)));
                annotationItemText1.setText("Project " + item.getProject().getProjectName());
                annotationItemText2.setText(item.getFormattedCreated_at());
                annotationItemText3.setText(item.getPictures().size() + "picture(s)");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, v);
                }
            });

            itemView.setOnLongClickListener(v -> {
                listener.onItemLongClick(item, v);
                return true;
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Annotation item, View v);
        boolean onItemLongClick(Annotation item, View v);
    }

    private final LayoutInflater inflater;

    private List<Annotation> annotations;

    private final OnItemClickListener listener;

    public AnnotationListAdapter(Context context, OnItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
        this.annotations = new ArrayList<>();
    }

    @Override
    public AnnotationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.list_item, parent, false);
        return new AnnotationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnotationViewHolder holder, int position) {
        if (annotations != null) {
            holder.bind(annotations.get(position), listener);
        }
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
        notifyDataSetChanged();
    }

    public void setAnnotation(Annotation annotation) {
        if (this.annotations == null) {
            this.annotations = new ArrayList<>();
        }
        this.annotations.add(annotation);
        notifyDataSetChanged();
    }

    public void clear(){
        this.annotations.clear();
        notifyDataSetChanged();
    }

    public void sort(int how){
        if (how == 1) {
            Collections.sort(this.annotations, (a, b) -> a.getCreated_at().compareTo(b.created_at));
            //van nieuw naar oud
        } else if (how == 0) {
            //van oudst naar nieuwst
            Collections.sort(this.annotations, (a, b) -> b.getCreated_at().compareTo(a.created_at));
        }
        notifyDataSetChanged();
    }

    public List<Annotation> getAnnotations() {
        if (annotations.size() > 0) {
            return this.annotations;
        } else{
            return null;
        }
    }


    @Override
    public int getItemCount() {
        if (annotations != null) {
            return annotations.size();
        } else {
            return 0;
        }
    }
}
