package com.example.posfoto.activities.AnnotationCreateProcess;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dsphotoeditor.sdk.activity.DsPhotoEditorActivity;
import com.dsphotoeditor.sdk.utils.DsPhotoEditorConstants;
import com.example.posfoto.R;
import com.example.posfoto.adapters.ImageGridAdapter;
import com.example.posfoto.sessions.SessionManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/**
 *
 * @TODO

*   3) de hoogte van de image objecten in de gridview aanpassen
 *
 */



public class PictureSelectActivity extends AppCompatActivity {

    FloatingActionButton btnOpenCamera;
    FloatingActionButton btnOpenGallery;

    RecyclerView imageGridView;

    Button btnNext;
    Button btnPrev;

    File photoFile;
    private ActionMode mActionMode;

    SessionManager session;

    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    public String photoFileName = timeStamp + ".jpg";
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1034;
    public final static int PICK_PHOTO_CODE = 1046;
    public final static int PHOTO_EDITOR_CODE = 1047;


    public final String APP_TAG = "POSFOTO";

    ArrayList<Uri> mArrayUri = new ArrayList<>();
    ArrayList<Bitmap> mBitmapSelected = new ArrayList<>();
    ImageGridAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_select);

        btnNext = findViewById(R.id.picture_select_next_button);
        btnPrev = findViewById(R.id.picture_select_back_button);
        btnOpenCamera = findViewById(R.id.btnOpenCamera);
        btnOpenGallery = findViewById(R.id.btnOpenGallery);
        imageGridView = findViewById(R.id.rvImages);

        session = new SessionManager(this);

        adapter = new ImageGridAdapter(mBitmapSelected, new ImageGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Bitmap item, int position, View v) {
                //enbiggen the image
                AlertDialog.Builder builder = new AlertDialog.Builder(PictureSelectActivity.this);
                builder.setTitle("Are you sure");
                builder.setMessage("Do you want to delete this image from the annotation?");

                // add a button
                builder.setPositiveButton("Delete", (dialogInterface, i) -> adapter.removeItem(position));
                builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

            }

            @Override
            public boolean onItemLongClick(Bitmap item, View v) {
                //add animation that makes it wiggle
                return true;
            }
        });


        imageGridView.setAdapter(adapter);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        imageGridView.setLayoutManager(gridLayoutManager);

        btnOpenCamera.setOnClickListener(this::onLaunchCamera);
        btnOpenGallery.setOnClickListener(this::onPickPhoto);

        btnNext.setOnClickListener(view -> {
                    Intent addTextIntent = new Intent(this, AddTextActivity.class);
                    addTextIntent.putExtra("pictures", mArrayUri);
                    startActivity(addTextIntent);

                }
        );

        btnPrev.setOnClickListener(view -> this.finish());

    }

    public void onLaunchCamera(View view) {
        Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoFile = getPhotoFileUri(photoFileName);

        Uri fileProvider = FileProvider.getUriForFile(PictureSelectActivity.this, "com.example.posfoto.fileProvider", photoFile);
        openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider);

        if (openCameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(openCameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

        }
    }


    public void onPickPhoto(View view) {
        Intent pickImageFromGalleryIntent = new Intent();

        pickImageFromGalleryIntent.setType("image/*");
        pickImageFromGalleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        pickImageFromGalleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        pickImageFromGalleryIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);


        if (pickImageFromGalleryIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(pickImageFromGalleryIntent,  "select pictures"), PICK_PHOTO_CODE);
        }

    }

    public Bitmap loadFromUri(android.net.Uri photoUri) {
        Bitmap image = null;
        try {
            // check version of Android on device
            if(Build.VERSION.SDK_INT > 27){
                // on newer versions of Android, use the new decodeBitmap method
                ImageDecoder.Source source = ImageDecoder.createSource(this.getContentResolver(), photoUri);
                image = ImageDecoder.decodeBitmap(source);
            } else {
                // support older versions of Android by using getBitmap
                image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }


    public File getPhotoFileUri(String fileName) {

        File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(APP_TAG, "failed to create directory");
        }

        File file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        Log.d("mediastorageTAG", mediaStorageDir.getParent() + File.separator + fileName);
        return file;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoFile.getAbsolutePath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PHOTO_EDITOR_CODE :

                if (data.getData() != null ) {
                    Uri outputUri = data.getData();
                    Bitmap test = loadFromUri(outputUri);


                    mArrayUri.add(outputUri);
                    mBitmapSelected.add(test);
                    adapter.setImages(mBitmapSelected);
                }

                break;
            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE  :
                if (resultCode == RESULT_OK) {
                    Bitmap takenImage = BitmapFactory.decodeFile(photoFile.getAbsolutePath());

                    Intent dsPhotoEditorIntent = new Intent(this, DsPhotoEditorActivity.class);
                    dsPhotoEditorIntent.setData(Uri.fromFile(getPhotoFileUri(photoFileName)));
                    dsPhotoEditorIntent.putExtra(DsPhotoEditorConstants.DS_PHOTO_EDITOR_OUTPUT_DIRECTORY, "test");
                    startActivityForResult(dsPhotoEditorIntent, PHOTO_EDITOR_CODE);


                } else {
                    Toast.makeText(this, "Picture wasn't taken", Toast.LENGTH_SHORT).show();
                }

                break;

                case PICK_PHOTO_CODE :
                    if (data != null) {

                        if (data.getData() != null) {
                            Uri photoUri = data.getData();

                            Intent dsPhotoEditorIntent = new Intent(this, DsPhotoEditorActivity.class);
                            dsPhotoEditorIntent.setData(photoUri);
                            dsPhotoEditorIntent.putExtra(DsPhotoEditorConstants.DS_PHOTO_EDITOR_OUTPUT_DIRECTORY, "test");
                            startActivityForResult(dsPhotoEditorIntent, PHOTO_EDITOR_CODE);


                        } else if (data.getClipData() != null) {
                            ClipData mClipData = data.getClipData();


                            for (int i = 0; i < mClipData.getItemCount(); i++) {
                                ClipData.Item item = mClipData.getItemAt(i);

                                Uri uri = item.getUri();

                                Intent dsPhotoEditorIntent = new Intent(this, DsPhotoEditorActivity.class);
                                dsPhotoEditorIntent.setData(uri);
                                dsPhotoEditorIntent.putExtra(DsPhotoEditorConstants.DS_PHOTO_EDITOR_OUTPUT_DIRECTORY, "test");
                                startActivityForResult(dsPhotoEditorIntent, PHOTO_EDITOR_CODE);
                            }

                        }

                        adapter.setImages(mBitmapSelected);

                    }
                break;

        }

    }
}
