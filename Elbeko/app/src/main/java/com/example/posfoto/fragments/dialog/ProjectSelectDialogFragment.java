package com.example.posfoto.fragments.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.example.posfoto.R;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.models.Project;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class ProjectSelectDialogFragment extends DialogFragment {

    private static String PROJECTS_KEY = "projects_key";
    private ArrayList<Project> projects = new ArrayList<>();
    private String title;
    private Project current_project;
    ProjectViewModel prvm;
    int selected;

    public ProjectSelectDialogFragment() {

    }

    ProjectSelectDialogListener list;

    public void ProjectSelectDialogListener(ProjectSelectDialogListener list) {
        this.list = list;
    }

    public static ProjectSelectDialogFragment newInstance(String title, ArrayList<Project> projects, Project current_project) {
        ProjectSelectDialogFragment frag = new ProjectSelectDialogFragment();
        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putSerializable("current_project", current_project);
        args.putParcelableArrayList(PROJECTS_KEY, projects);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.projects = getArguments().getParcelableArrayList(PROJECTS_KEY);
            this.title = getArguments().getString("Title");
            this.current_project = (Project) getArguments().getSerializable("current_project");
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
        builder.setNeutralButton(getResources().getString(R.string.cancel), (dialogInterface, i) -> {
            dialogInterface.dismiss();

        });

        ArrayList<String> projectsToDisplay = new ArrayList<>();
        for (int i = 0; i < projects.size(); i++) {
            projectsToDisplay.add(projects.get(i).getProjectName());
        }

        //needs listadapter
        builder.setPositiveButton(getResources().getString(R.string.ok), (dialogInterface, i) -> {
            String projName = projectsToDisplay.get(selected);
            Project selectedProject = this.projects.stream().filter(p -> p.getProjectName().equals(projName)).findFirst().get();
            list.onFinishedSelect(selectedProject);
        });


        int index = current_project != null ? projectsToDisplay.indexOf(current_project.getProjectName()) : 0;
        if (index == -1) builder.setTitle("No project selected"); else builder.setTitle("Change current project");

        builder.setSingleChoiceItems(projectsToDisplay.toArray(new String[projectsToDisplay.size()]), index, (dialogInter, i) -> {
                    selected = i;
                });
        return builder.create();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * INTERFACE
     */

    public interface ProjectSelectDialogListener {
        void onFinishedSelect(Project project);
        //set current project
    }

}
