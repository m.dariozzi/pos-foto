package com.example.posfoto.datalayer;

import android.content.Context;

import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.DAOs.AnnotationDao;
import com.example.posfoto.models.DAOs.AuthorDao;
import com.example.posfoto.models.DAOs.ProjectDao;
import com.example.posfoto.models.Generaldata;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.Subproject;
import com.example.posfoto.models.TypeConverters.DateTypeConverter;

import javax.annotation.Nonnull;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;


/**
 * ROOM
 * Database init and config
 */

@Database(entities = {Annotation.class, Author.class, Project.class, Generaldata.class, Subproject.class},  version = 48)
@TypeConverters({DateTypeConverter.class})
public abstract class AnnotationRoomDatabase extends RoomDatabase {

    public abstract AnnotationDao annotationDao();

    public abstract AuthorDao authorDao();

    public abstract ProjectDao projectDao();

    private static volatile AnnotationRoomDatabase INSTANCE;

    public static AnnotationRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AnnotationRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AnnotationRoomDatabase.class, "POS_FOTO_Database")
                            .addMigrations(MIGRATION_1_39)
                            .addMigrations(MIGRATION_39_40)
                            .addMigrations(Migration_40_41)
                            .addMigrations(Migration_41_42)
                            .addMigrations(Migration_42_43)
                            .addMigrations(Migration_43_44)
                            .addMigrations(Migration_44_45)
                            .addMigrations(Migration_45_46)
                            .addMigrations(Migration_46_47)
                            .addMigrations(Migration_47_48)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

//    migraties zijn nog niet nodig omdat we een in een testomgeving zitten, de eerste migratie moet geschreven worden wanneer de app effectief gebruikt gaat worden
//     waarna de data bewaard moet blijven als de data aangepast wordt.


    static final Migration MIGRATION_1_39 = new Migration(1, 39) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //om de shit te doen werken
        }
    };

    static final Migration MIGRATION_39_40 = new Migration(39, 40) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //om de shit te doen werken
        }
    };

    /**
     * ADD NEW migrations starting from vs 40
     */

    static final Migration Migration_40_41 = new Migration(40, 41) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database){
                //alter table authors and projects
            database.execSQL("CREATE TABLE new_projects(" +
                            "projectId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                            "project_name TEXT," +
                            "project_updated_at INTEGER," +
                            "project_POS_internal_code TEXT," +
                            "project_manager_id INTEGER NOT NULL," +
                            "FOREIGN KEY(`project_manager_id`) REFERENCES `authors`(`authorId`) ON UPDATE NO ACTION ON DELETE NO ACTION)");

            database.execSQL("INSERT INTO new_projects (projectId, project_name, project_updated_at, project_POS_internal_code, project_manager_id)" +
                    "SELECT * FROM projects");

            database.execSQL("DROP table projects");
            database.execSQL("ALTER TABLE new_projects RENAME TO projects");

            database.execSQL("CREATE TABLE new_annotations(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "pictures TEXT," +
                    "text TEXT," +
                    "author_id INTEGER NOT NULL," +
                    "project_id INTEGER NOT NULL," +
                    "status INTEGER," +
                    "created_at INTEGER," +
                    "authorId INTEGER," +
                    "firstname TEXT," +
                    "lastname TEXT," +
                    "email TEXT," +
                    "username TEXT," +
                    "password TEXT," +
                    "role INTEGER," +
                    "author_updated_at INTEGER," +
                    "author_POS_internal_code TEXT," +
                    "projectId INTEGER," +
                    "project_name TEXT," +
                    "project_updated_at INTEGER," +
                    "project_POS_internal_code TEXT," +
                    "project_manager_id INTEGER," +
                    "FOREIGN KEY(`author_id`) REFERENCES `authors`(`authorId`) ON UPDATE NO ACTION ON DELETE NO ACTION," +
                    "FOREIGN KEY(`project_id`) REFERENCES `projects`(`projectId`) ON UPDATE NO ACTION ON DELETE NO ACTION )");

          database.execSQL("INSERT INTO new_annotations (id, pictures, text, author_id, project_id, status, created_at, author_id, project_id)" +
                  "SELECT id, pictures, text, author_id, project_id, status, created_at, author_id, project_id from annotations");
          database.execSQL("DROP TABLE annotations");


          database.execSQL("ALTER TABLE new_annotations RENAME TO annotations");
        }
    };

    static final Migration Migration_41_42 = new Migration(41, 42) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE new_annotations(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "pictures TEXT," +
                    "text TEXT," +
                    "author_id INTEGER NOT NULL," +
                    "project_id INTEGER NOT NULL," +
                    "status INTEGER," +
                    "created_at INTEGER," +
                    "authorId INTEGER," +
                    "firstname TEXT," +
                    "lastname TEXT," +
                    "email TEXT," +
                    "username TEXT," +
                    "password TEXT," +
                    "role INTEGER," +
                    "author_updated_at INTEGER," +
                    "author_POS_internal_code TEXT," +
                    "projectId INTEGER," +
                    "project_name TEXT," +
                    "project_updated_at INTEGER," +
                    "project_POS_internal_code TEXT," +
                    "project_manager_id INTEGER," +
                    "FOREIGN KEY(`author_id`) REFERENCES `authors`(`authorId`) ON UPDATE NO ACTION ON DELETE CASCADE," +
                    "FOREIGN KEY(`project_id`) REFERENCES `projects`(`projectId`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            database.execSQL("INSERT INTO new_annotations (id, pictures, text, author_id, project_id, status, created_at, author_id, project_id)" +
                    "SELECT id, pictures, text, author_id, project_id, status, created_at, author_id, project_id from annotations");
            database.execSQL("DROP TABLE annotations");


            database.execSQL("ALTER TABLE new_annotations RENAME TO annotations");
        }
    };

    static final Migration Migration_42_43 = new Migration(42, 43) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE new_projects(" +
                    "projectId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "project_name TEXT," +
                    "project_updated_at INTEGER," +
                    "project_POS_internal_code TEXT," +
                    "project_manager_id INTEGER NOT NULL," +
                    "FOREIGN KEY(`project_manager_id`) REFERENCES `authors`(`authorId`) ON UPDATE NO ACTION ON DELETE CASCADE)");

            database.execSQL("INSERT INTO new_projects (projectId, project_name, project_updated_at, project_POS_internal_code, project_manager_id)" +
                    "SELECT * FROM projects");

            database.execSQL("DROP table projects");
            database.execSQL("ALTER TABLE new_projects RENAME TO projects");
        }
    };

    static final Migration Migration_43_44 = new Migration(43, 44) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE new_projects(" +
                    "projectId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "project_name TEXT," +
                    "project_updated_at INTEGER," +
                    "project_POS_internal_code TEXT," +
                    "project_manager_id INTEGER NOT NULL," +
                    "FOREIGN KEY(`project_manager_id`) REFERENCES `authors`(`authorId`) ON UPDATE NO ACTION ON DELETE NO ACTION)");

            database.execSQL("INSERT INTO new_projects (projectId, project_name, project_updated_at, project_POS_internal_code, project_manager_id)" +
                    "SELECT * FROM projects");

            database.execSQL("DROP table projects");
            database.execSQL("ALTER TABLE new_projects RENAME TO projects");
        }
    };

    static final Migration Migration_44_45 = new Migration(44, 45) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE new_annotations(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "pictures TEXT," +
                    "text TEXT," +
                    "author_id INTEGER NOT NULL," +
                    "project_id INTEGER NOT NULL," +
                    "status INTEGER," +
                    "created_at INTEGER," +
                    "projectId INTEGER," +
                    "project_name TEXT," +
                    "project_updated_at INTEGER," +
                    "project_POS_internal_code TEXT," +
                    "project_manager_id INTEGER," +
                    "FOREIGN KEY(`project_id`) REFERENCES `projects`(`projectId`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            database.execSQL("INSERT INTO new_annotations (id, pictures, text, author_id, project_id, status, created_at, author_id, project_id)" +

                    "SELECT id, pictures, text, author_id, project_id, status, created_at, author_id, project_id from annotations");
            database.execSQL("DROP TABLE annotations");
            database.execSQL("ALTER TABLE new_annotations RENAME TO annotations");
        }
    };


    static final Migration Migration_45_46 = new Migration(45, 46) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE new_annotations(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "pictures TEXT," +
                    "text TEXT," +
                    "author_id INTEGER NOT NULL," +
                    "project_id INTEGER NOT NULL," +
                    "status INTEGER," +
                    "created_at INTEGER," +
                    "projectId INTEGER," +
                    "project_name TEXT," +
                    "project_updated_at INTEGER," +
                    "project_POS_internal_code TEXT," +
                    "project_manager_id INTEGER," +
                    "FOREIGN KEY(`project_id`) REFERENCES `projects`(`projectId`) ON UPDATE NO ACTION ON DELETE NO ACTION )");

            database.execSQL("INSERT INTO new_annotations (id, pictures, text, author_id, project_id, status, created_at, author_id, project_id)" +
                    "SELECT id, pictures, text, author_id, project_id, status, created_at, author_id, project_id from annotations");
            database.execSQL("DROP TABLE annotations");
            database.execSQL("ALTER TABLE new_annotations RENAME TO annotations");
        }
    };

    static final Migration Migration_46_47 = new  Migration(46, 47) {
            @Override
            public void migrate(@Nonnull SupportSQLiteDatabase database){
                database.execSQL("CREATE TABLE new_annotations(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                        "pictures TEXT," +
                        "text TEXT," +
                        "author_id INTEGER NOT NULL," +
                        "project_id INTEGER NOT NULL," +
                        "status INTEGER," +
                        "created_at INTEGER)");

                database.execSQL("INSERT INTO new_annotations (id, pictures, text, author_id, project_id, status, created_at)" +
                        "SELECT id, pictures, text, author_id, project_id, status, created_at from annotations");
                database.execSQL("DROP TABLE annotations");
                database.execSQL("ALTER TABLE new_annotations RENAME TO annotations");
        }
    };


    static final Migration Migration_47_48 = new Migration(47, 48) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS subprojects" +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " `name` TEXT," +
                    " `pos_internal_code` INTEGER NOT NULL," +
                    " `linked_project_id` INTEGER NOT NULL)");

        }
    };
}


