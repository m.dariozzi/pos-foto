package com.example.posfoto.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Serverdata {

    @SerializedName("authorId")
    @Expose
    private String authorId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("projectId")
    @Expose
    private String projectId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("author_internal_code")
    @Expose
    private String authorInternalCode;
    @SerializedName("project_internal_code")
    @Expose
    private String projectInternalCode;
    @SerializedName("numberOfPictures")
    @Expose
    private String numberOfPictures;
    @SerializedName("pictures")
    @Expose
    private List<String> pictures = null;

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFormattedCreated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.getDefault());
        return dateFormat.format(createdAt);
    }

    public Date getCreatedAtAsDate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.getDefault());
        return dateFormat.parse(createdAt.replaceAll("\\p{Cntrl}", ""));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthorInternalCode() {
        return authorInternalCode;
    }

    public void setAuthorInternalCode(String authorInternalCode) {
        this.authorInternalCode = authorInternalCode;
    }

    public String getProjectInternalCode() {
        return projectInternalCode;
    }

    public void setProjectInternalCode(String projectInternalCode) {
        this.projectInternalCode = projectInternalCode;
    }

    public String getNumberOfPictures() {
        return numberOfPictures;
    }

    public void setNumberOfPictures(String numberOfPictures) {
        this.numberOfPictures = numberOfPictures;
    }

    public ArrayList<String> getPictures() {
        return (ArrayList) pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }
}