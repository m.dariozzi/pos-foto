package com.example.posfoto.activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.posfoto.R;
import com.example.posfoto.adapters.AuthorSpinnerArrayAdapter;
import com.example.posfoto.adapters.ProjectSpinnerArrayAdapter;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.helper.PictureHelper;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

;

public class AnnotationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final int PICK_IMAGE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;

    FloatingActionButton btnUseCamera;
    FloatingActionButton btnUseGallery;
    ImageView imgThumbnail;
    EditText annotationText;
    String currentPhotoPath;

    Spinner spnSelectAuthor;
    Spinner spnSelectProject;

    Bitmap imageToDisplay;
    Uri imageUri;
    ArrayList<String> imageUriStrings = new ArrayList<>();
    public int pictureAmount = 0;

    private AnnotationViewModel annotationViewModel;

    AuthorSpinnerArrayAdapter authorAdapter;
    ProjectSpinnerArrayAdapter projectAdapter;

    Annotation edit_annotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annotation);
        edit_annotation = (Annotation) getIntent().getSerializableExtra("edit_annotation");

        btnUseCamera = findViewById(R.id.btnUseCamera);
        btnUseGallery = findViewById(R.id.btnUseGallery);
        imgThumbnail = findViewById(R.id.imageView2);
        annotationText = findViewById(R.id.annotation_text);

        spnSelectAuthor = findViewById(R.id.spnSelectAuthor);
        spnSelectProject = findViewById(R.id.spnSelectProject);

        annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final Date current_date = new Date();

        /**
         * fetch data from the server or local db to fill spinners
         */
        authorAdapter = new AuthorSpinnerArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item);
        projectAdapter = new ProjectSpinnerArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item);


        annotationViewModel.getAllAuthors().observe(this, new Observer<List<Author>>() {
            @Override
            public void onChanged(List<Author> authors) {
                authors.add(0, new Author(-1, "choose author", "", "", "", "" , Author.Role.USER, current_date, ""));
                authorAdapter.setAuthors(authors);
            }
        });

        annotationViewModel.getAllProjects().observe(this, new Observer<List<Project>>() {
            @Override
            public void onChanged(List<Project> projects) {
                projects.add(0, new Project(-1, "choose project", "04774", current_date, 2));
                projectAdapter.setProjects(projects);

            }
        });

        authorAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnSelectAuthor.setAdapter(authorAdapter);
        spnSelectAuthor.setOnItemSelectedListener(this);

        projectAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnSelectProject.setAdapter(projectAdapter);
        spnSelectProject.setOnItemSelectedListener(this);

        btnUseCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        btnUseGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchSelectPictureFromGalleryIntent();
            }
        });

        annotationText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


       if (edit_annotation != null) {
            imageUri = Uri.parse(edit_annotation.getPictureByIndex(0));
            Glide.with(this).load(imageUri).placeholder(R.drawable.ic_image_placeholder).into(imgThumbnail);
            spnSelectAuthor.setSelection(edit_annotation.getAuthorId());
            spnSelectProject.setSelection(edit_annotation.getProjectId());
            annotationText.setText(edit_annotation.getText(), TextView.BufferType.EDITABLE);

       }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_annotation_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                finish();
                return true;
            case R.id.save_annotation_row:
                Date current_date = new Date();
//                imageUriStrings.add(imageUri.toString());

                int author_position = spnSelectAuthor.getSelectedItemPosition();
                int project_position = spnSelectProject.getSelectedItemPosition();

                try {
                    if (edit_annotation != null) {
                        edit_annotation.setPictures(imageUriStrings);
                        edit_annotation.setText(annotationText.getText().toString());
                        edit_annotation.setAuthorId(authorAdapter.getAuthors().get(author_position).getAuthorId());
                        edit_annotation.setProjectId(projectAdapter.getProjects().get(project_position).getProjectId());
                        annotationViewModel.update(edit_annotation);
                    } else {
                        Annotation newAnnotation = new Annotation(
                                authorAdapter.getAuthors().get(author_position),
                                projectAdapter.getProjects().get(project_position),
                                imageUriStrings,
                                annotationText.getText().toString(),
                                authorAdapter.getAuthors().get(author_position).getAuthorId(),
                                projectAdapter.getProjects().get(project_position).getProjectId(),
                                current_date
                        );
                        annotationViewModel.insert(newAnnotation);
                    }

                    setResult(Activity.RESULT_OK);

                    finish();
                    return true;
                } catch (Exception e) {
                    finish();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Bitmap bitmap = (Bitmap) savedInstanceState.getParcelable("bitmap");
        imgThumbnail.setImageBitmap(bitmap);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void dispatchSelectPictureFromGalleryIntent() {
        Intent selectPictureFromGalleryIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        selectPictureFromGalleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        selectPictureFromGalleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
        selectPictureFromGalleryIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(selectPictureFromGalleryIntent, "Select Picture"), PICK_IMAGE);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (data == null) {
                return;
            }

            switch (requestCode) {
                case PICK_IMAGE:
                    if (data.getClipData() != null) {
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            ClipData.Item item = data.getClipData().getItemAt(i);
                            imageUriStrings.add(item.getUri().toString());

                        }
                        Glide.with(this).load(imageUriStrings).placeholder(R.drawable.ic_photo_library).into(imgThumbnail);
                    } else {
                        imageUri = data.getData();
                        imageUriStrings.add(imageUri.toString());
                        imgThumbnail.setImageURI(imageUri);
                    }

                    Log.d("images test", "onActivityResult: " + imageUriStrings.toArray().length);
                    break;

                case REQUEST_IMAGE_CAPTURE:
                    imageUri = Uri.fromFile(new File(currentPhotoPath));
                    imageUriStrings.add(imageUri.toString());
                    imageToDisplay = getBitmap(currentPhotoPath);

                    imgThumbnail.setImageBitmap(imageToDisplay);
                    PictureHelper.galleryAddPic(getApplicationContext(), currentPhotoPath);
                    pictureAmount++;

                    Log.d("images test", "onActivityResult: " + imageUriStrings.toArray().length);

                    //@TODO een dowhile loop instellen.
                    if (pictureAmount < 10) {
                        dispatchTakePictureIntent();
                    }
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
//            Toast.makeText(this, "" + authorAdapter.getAuthors().get(position).getAuthorId(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (annotationText.getText().toString().isEmpty()) {
            menu.getItem(0).setEnabled(false);
        } else {
            menu.getItem(0).setEnabled(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }


    /**
     * RESUABLE METHODS
     *
     * @TODO these methods have to be moved somewhere else, like helper classes somewhere.
     */

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getApplicationContext(), "Error while saving picture.", Toast.LENGTH_LONG).show();

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.FileProvider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private Bitmap getBitmap(String path) {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1800000; // 1.2MP
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }
}
