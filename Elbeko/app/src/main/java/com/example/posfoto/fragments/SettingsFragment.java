package com.example.posfoto.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.posfoto.BuildConfig;
import com.example.posfoto.R;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.fragments.dialog.ProjectSelectDialogFragment;
import com.example.posfoto.fragments.dialog.ServerSelectDialogFragment;
import com.example.posfoto.models.Project;
import com.example.posfoto.sessions.SessionManager;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment implements ProjectSelectDialogFragment.ProjectSelectDialogListener, ServerSelectDialogFragment.ServerSelectDialogInterface {

    View view;
    Button btnSelectProjects;
    Button btnSelectServer;
    ArrayList<Project> projects = new ArrayList<>();
    ProjectViewModel pvrm;
    SessionManager session;

    public SettingsFragment() {
        // Required empty public constructor
    }


    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        setHasOptionsMenu(true);
        return view;

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnSelectProjects = view.findViewById(R.id.BtnSettingsFirst);
        btnSelectServer = view.findViewById(R.id.BtnSettingsSecond);

        pvrm = new ViewModelProvider(this).get(ProjectViewModel.class);
        session = new SessionManager(getContext());

        pvrm.getAllProjects().observe(getViewLifecycleOwner(), projectList -> {
            this.projects.clear();
            this.projects.addAll(projectList);
        });

        btnSelectProjects.setOnClickListener(view -> {  showDialog(projects, session.getSelectedProject()); });


        btnSelectServer.setOnClickListener(view -> {
            FragmentManager fm = getParentFragmentManager();
            ServerSelectDialogFragment serverSelectDialogFragment = ServerSelectDialogFragment.newInstance("SELECT server", BuildConfig.URL_ARRAY, session.getSelectedServer());

            serverSelectDialogFragment.ServerSelectDialogListener(SettingsFragment.this);
            serverSelectDialogFragment.show(fm, "fragment_edit_server");

        });

    }

    private void showDialog(ArrayList<Project> projects, Project current_project) {
        FragmentManager fm = getParentFragmentManager();
        ProjectSelectDialogFragment projectSelectDialogFragment = ProjectSelectDialogFragment.newInstance(getResources().getString(R.string.elect_project_dialog_title), projects, current_project);
        projectSelectDialogFragment.ProjectSelectDialogListener(SettingsFragment.this);
        projectSelectDialogFragment.show(fm, "fragment_edit_name");
    }

    @Override
    public void onFinishedSelect(Project project) {
        session.setSelectedProject(project);
        NavigationView navigationView = getActivity().findViewById(R.id.navigationView);

        View headerView = navigationView.getHeaderView(0);
        TextView txtCurrentProject = headerView.findViewById(R.id.txtSideBarHeaderSelectedProject);
        if (session.getSelectedProject() != null) {
            txtCurrentProject.setText("current project: " + session.getSelectedProject().getProjectName());

        } else {
            txtCurrentProject.setText("no project selected");
        }
    }

    @Override
    public void onFinishedSelect(int index) {

        session.setSelectedServer(BuildConfig.URL_ARRAY[index]);
        NavigationView navigationView =  getActivity().findViewById(R.id.navigationView);

        View headerView = navigationView.getHeaderView(0);
        TextView txtCurrentProject = headerView.findViewById(R.id.txtSidebarHeaderServerConnection);
        if (session.getSelectedServer() != null) {
            txtCurrentProject.setText(session.getSelectedServer());
        } else {
            txtCurrentProject.setText("No server Connected");
        }
    }
}