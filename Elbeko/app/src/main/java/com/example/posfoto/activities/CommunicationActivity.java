package com.example.posfoto.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.posfoto.R;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.Serverdata;
import com.example.posfoto.network.APIs.AuthorService;
import com.example.posfoto.network.APIs.ProjectService;
import com.example.posfoto.network.RetrofitClientInstance;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CommunicationActivity extends AppCompatActivity {

    private Button btnSendAnnotations;
    private Button btnSendProjects;
    private Button btnSendAuthors;

    private Button btnGetAnnotations;
    private Button btnGetAuthors;
    private Button btnGetProjects;

    private ViewModel annoViewmodel;

    private List<Annotation> annotations;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);

        btnSendAnnotations = findViewById(R.id.btnTestSendAnnotations);
        btnSendAuthors = findViewById(R.id.btnTestSendAuthors);
        btnSendProjects = findViewById(R.id.btnTestSendProjects);

        btnGetAnnotations = findViewById(R.id.btnTestGetAnnotations);
        btnGetAuthors = findViewById(R.id.btnTestGetAuthors);
        btnGetProjects = findViewById(R.id.btnTestGetProjects);


        AnnotationViewModel m_annotationViewM = new ViewModelProvider(this).get(AnnotationViewModel.class);
        AuthorViewModel m_authorViewM = new ViewModelProvider(this).get(AuthorViewModel.class);
        ProjectViewModel m_projectViewM = new ViewModelProvider(this).get(ProjectViewModel.class);

        btnGetAnnotations.setOnClickListener(v -> {
            m_annotationViewM.getAnnotationsFromServer().enqueue(new Callback<List<Serverdata>>() {
                @Override
                public void onResponse(Call<List<Serverdata>> call, Response<List<Serverdata>> response) {
                    Toast.makeText(CommunicationActivity.this, response.code() + response.message(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(CommunicationActivity.this, "ok", Toast.LENGTH_SHORT).show();


                    if (response.isSuccessful()) {


                        List<Author> authors = new ViewModelProvider(CommunicationActivity.this).get(AuthorViewModel.class).getAllAuthors().getValue();
                        List<Project> projects = new ViewModelProvider(CommunicationActivity.this).get(ProjectViewModel.class).getAllProjects().getValue();

//                        annotations = DatabaseHelper.synchronizeDatabase(response.body(), getApplicationContext(), authors, projects);
                    }
                }

                @Override
                public void onFailure(Call<List<Serverdata>> call, Throwable t) {

                    t.printStackTrace();
                    Toast.makeText(CommunicationActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        });


        btnGetAuthors.setOnClickListener(v -> {
            Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(this);
            AuthorService api = retrofit.create(AuthorService.class);

           api.getAllAuthors().enqueue(new Callback<List<Author>>() {
               @Override
               public void onResponse(Call<List<Author>> call, Response<List<Author>> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(CommunicationActivity.this, "ok", Toast.LENGTH_SHORT).show();
                   } else {
                       Toast.makeText(CommunicationActivity.this, "nok", Toast.LENGTH_SHORT).show();
                   }

               }

               @Override
               public void onFailure(Call<List<Author>> call, Throwable t) {
                    t.printStackTrace();
                   Toast.makeText(CommunicationActivity.this, "nok", Toast.LENGTH_SHORT).show();
               }
           });
        });

        btnGetProjects.setOnClickListener(v -> {
            Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(this);
            ProjectService api = retrofit.create(ProjectService.class);

            api.getAllProjects().enqueue(new Callback<List<Project>>() {
                @Override
                public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {

                }

                @Override
                public void onFailure(Call<List<Project>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        });
    }
}
