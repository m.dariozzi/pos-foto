package com.example.posfoto.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.posfoto.R;
import com.example.posfoto.activities.AnnotationActivity;
import com.example.posfoto.adapters.AnnotationListAdapter;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.fragments.dialog.FilterModalFragment;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.Serverdata;
import com.example.posfoto.sessions.SessionManager;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
 public class PageFragment extends Fragment implements FilterModalFragment.FilterModalFragmentListener {


    View view;
    private AnnotationListAdapter adapter;
    private RecyclerView recyclerView;
    private AnnotationViewModel annotationViewModel;
    private SessionManager session;
    private ActionMode mActionMode;
    List<Annotation> annotations = new ArrayList<>();
    public static final int EDIT_ANNOTATION_ACTIVITY_REQUEST_CODE = 2;

    FilterModalFragment frag = new FilterModalFragment(this);

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PAGE = "ARG_PAGE";

    private final int LOCAL_DATA_SOURCE = 1;
    private final int SERVER_DATA_SOURCE = 2;

    // TODO: Rename and change types of parameters
    private int mPage;

    public PageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PageFragment newInstance(int page) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPage = getArguments().getInt(ARG_PAGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        recyclerView = view.findViewById(R.id.annotation_list_view);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        session = new SessionManager(getContext());

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.my_toolbar);
        toolbar.setTitle("Annotations");
        setHasOptionsMenu(true);

        adapter = new AnnotationListAdapter(getContext(), new AnnotationListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Annotation item, View v) {

                //pass the item to the new fragment
                Fragment fragment = DetailFragment.newInstance(item, mPage);

                FragmentTransaction fts = getActivity().getSupportFragmentManager().beginTransaction();
                fts.setCustomAnimations(R.anim.slide_in_left, R.anim.left_to_right_slide_out);
                fts.replace(R.id.your_placeholder, fragment);
                fts.addToBackStack("local_annotations");
                fts.commit();


            }

            @Override
            public boolean onItemLongClick(Annotation item, View v) {
                if (mActionMode != null) {
                    return false;
                }

                mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback);
                mActionMode.setTag(item);
                return true;
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.notifyDataSetChanged();

        annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);
        HashMap<String, String> user = session.getUserDetails();


        /**
         * @TODO de annotaties voor de gebruiker van de server halen.
         * 2) een tab view met de lokale verstuurde en de remote bestaande
         */

        if (mPage == LOCAL_DATA_SOURCE) {

            annotationViewModel.getSentAnnotations().observe(getViewLifecycleOwner(), annotationList -> {
                for (Annotation anno :
                        annotationList) {
                    new ViewModelProvider(this).get(AuthorViewModel.class).getAllAuthorsById(anno.getAuthorId()).observe(getViewLifecycleOwner(), author-> {
                        anno.setAuthor(author.get(0));
                        new ViewModelProvider(this).get(ProjectViewModel.class).getProjectById(anno.getProjectId()).observe(getViewLifecycleOwner(), projectList ->  {
                            anno.setProject(projectList.get(0));
                            adapter.setAnnotation(anno);
                            adapter.sort(0);
                            adapter.notifyDataSetChanged();
                        });
                    });
                }

            });

        } else if (mPage == SERVER_DATA_SOURCE) {
            adapter.clear();
            synchronize(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    
    


    private void synchronize(AnnotationListAdapter adapter) {
        List<Author> authors = new ArrayList<>();
        List<Project> projects = new ArrayList<>();
        AuthorViewModel authorViewModel = new ViewModelProvider(this).get(AuthorViewModel.class);
        ProjectViewModel projectViewModel = new ViewModelProvider(this).get(ProjectViewModel.class);

        authorViewModel.getAllAuthors().observe(getViewLifecycleOwner(), authors::addAll);
        projectViewModel.getAllProjects().observe(getViewLifecycleOwner(), projectList -> {
            projects.addAll(projectList);

            Call<List<Serverdata>> annos;

            if (session.getSelectedProject() == null) {
                annos = annotationViewModel.getAnnotationsFromServer();
            } else {
                annos = annotationViewModel.getAnnotationsByProject_id(String.valueOf(session.getSelectedProject().getProjectId()));
            }

            annos.enqueue(new Callback<List<Serverdata>>() {
                @Override
                public void onResponse(Call<List<Serverdata>> call, Response<List<Serverdata>> response) {
                    try {
                        for (Serverdata data:
                                response.body()) {
                            Project m_project;
                            Author m_author = authors.stream().filter(a -> a.getAuthorId() == Integer.parseInt(data.getAuthorId())).findFirst().get();
                            m_project = projects.stream().filter(p ->  p.getProjectId() == Integer.parseInt(data.getProjectId())).findFirst()
                                     .orElse(new Project(6999, "no project seleted", "6999", data.getCreatedAtAsDate(), m_author.getAuthorId()));


                                adapter.setAnnotation(new Annotation(
                                        m_author,
                                        m_project,
                                        data.getPictures(),
                                        data.getText(),
                                        Integer.parseInt(data.getAuthorId()),
                                        Integer.parseInt(data.getProjectId()),
                                        data.getCreatedAtAsDate()));

                                adapter.sort(0);
                                adapter.notifyDataSetChanged();


                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.d("TAG", "onResponse: DATES ZIJN BULLSHIT");
                        }
                    }

                @Override
                public void onFailure(Call<List<Serverdata>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_filter, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_show_filter:
                FragmentManager fm = getParentFragmentManager();

                ArrayList<Project> projects = new ArrayList<>();
                for (Annotation annotations: adapter.getAnnotations()) {
                    projects.add(annotations.getProject());
                }
                FilterModalFragment filterModalFragment = FilterModalFragment.newInstance("Annotations List Filter", projects);

                filterModalFragment.show(fm, "fragment_edit_name");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.annotation_options_menu, menu);
            mode.setTitle("Choose option");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }





        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Annotation anno = (Annotation) mode.getTag();

            switch (item.getItemId()) {
                case R.id.edit_item:
                    Intent editAnnotationIntent = new Intent(getContext(), AnnotationActivity.class);
                    editAnnotationIntent.putExtra("edit_annotation", anno);
                    editAnnotationIntent.addCategory(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(editAnnotationIntent, EDIT_ANNOTATION_ACTIVITY_REQUEST_CODE);
                    return true;
                case R.id.delete_annotation:
                    annotationViewModel.deleteById(anno.getId());
                    mActionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };


    @Override
    public void onSelectFilter() {
        Log.d("TAG", "test");
    }
}