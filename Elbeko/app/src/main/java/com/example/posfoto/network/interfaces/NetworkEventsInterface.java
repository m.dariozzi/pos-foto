package com.example.posfoto.network.interfaces;

import android.view.View;

import com.example.posfoto.models.Annotation;

public interface NetworkEventsInterface {
    void onSuccess(Annotation item, View ItemView);
    void onError(retrofit2.Response<String> response);
}
