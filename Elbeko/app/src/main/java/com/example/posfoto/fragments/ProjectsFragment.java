package com.example.posfoto.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.posfoto.activities.EditProjectActivity;
import com.example.posfoto.R;
import com.example.posfoto.adapters.ProjectListAdapter;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.models.Project;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsFragment extends Fragment {

    View view;
    private ProjectListAdapter adapter;
    private RecyclerView recyclerView;
    private ProjectViewModel viewModel;
    private FloatingActionButton ftlBtnAddProject;
    private ActionMode mActionMode;
    private int EDIT_PROJECT_ACTIVITY_REQUEST_CODE = 4;

    public ProjectsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_projects, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        recyclerView = view.findViewById(R.id.project_list_view);
        ftlBtnAddProject = view.findViewById(R.id.fltBtnAddProjects);



        final ProjectListAdapter adapter = new ProjectListAdapter(getContext(), new ProjectListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Project item, View v) {
                Toast.makeText(getContext(), "azeaze", Toast.LENGTH_LONG);
            }

            @Override
            public boolean onItemLongClick(Project item, View v) {
                if (mActionMode != null) {
                    return false;
                }

                mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeUserCallback);
                mActionMode.setTag(item);
                return true;
            }

        });

        ftlBtnAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newProjectIntent = new Intent(getActivity(), EditProjectActivity.class);
                newProjectIntent.putExtra("current_projects", (Serializable) adapter.getProjects().get(adapter.getProjects().size() - 1));
                startActivity(newProjectIntent);
            }
        });


        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        viewModel = new ViewModelProvider(this).get(ProjectViewModel.class);
        viewModel.getAllProjects().observe(getViewLifecycleOwner(), projects -> adapter.setProjects(projects));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private ActionMode.Callback mActionModeUserCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.annotation_options_menu, menu);
            mode.setTitle("Choose option for project");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Project project = (Project) mode.getTag();
            switch (item.getItemId()) {
                case R.id.edit_item:
                    Intent editProjectIntent = new Intent(getContext(), EditProjectActivity.class);
                    editProjectIntent.putExtra("edit_project", (Serializable) project);
                    editProjectIntent.addCategory(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(editProjectIntent, EDIT_PROJECT_ACTIVITY_REQUEST_CODE);
                    mActionMode.finish();
                    return true;
                case R.id.delete_annotation:
                    /*
                    @TODO: eerst proberen sturen naar de server
                    daarna alle annotations ophalen en dan pas de user + annotations verwijderen.
                     */


                    viewModel.deleteById(project.getProjectId());
                    mActionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
}
