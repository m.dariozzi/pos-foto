package com.example.posfoto.models.DAOs;

import com.example.posfoto.models.Generaldata;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface GeneralDataDao {

    @Query("select * from generaldata")
    List<Generaldata> getAllRows();

    @Query("select * from generaldata where name = :name")
    List<Generaldata> getRowByName(String name);

    @Query("select * from generaldata where value = :value")
    List<Generaldata> getRowByValue(String value);

    @Query("select * from generaldata where id = :id")
    List<Generaldata> getRowById(int id);

    @Insert
    void insert(Generaldata data);

    @Delete
    @Query("delete from generaldata")
    void deleteAll();

    @Query("delete from generaldata where id = :id")
    void delete(int id);

    @Update
    void update(Generaldata data);

}
