package com.example.posfoto.datalayer;

import android.os.AsyncTask;

import com.example.posfoto.models.Author;
import com.example.posfoto.models.DAOs.AnnotationDao;
import com.example.posfoto.models.DAOs.AuthorDao;
import com.example.posfoto.models.DAOs.ProjectDao;
import com.example.posfoto.models.Project;

import java.util.Date;


public class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    private final AnnotationDao annotationDao;
    private final AuthorDao authorDao;
    private final ProjectDao projectDao;

    PopulateDbAsync(AnnotationRoomDatabase db) {
        annotationDao = db.annotationDao();
        authorDao = db.authorDao();
        projectDao = db.projectDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {
        annotationDao.deleteAll();
        projectDao.deleteAll();
        authorDao.deleteAll();
//
        Date current_date = new Date();
//
        Author admin = new Author(1, "ad", "min", "admin@admin.admin", "admin", "admin", Author.Role.MASTER, current_date, "04711");
        Author author1 = new Author(2, "Matteo", "Dariozzi", "matteo.dariozzi@hotmail.com", "md", "test", Author.Role.USER, current_date, "04711");
        Author author2 = new Author(3, "Cautino", "Dariozzi", "cd@ecs_website.com", "cd", "test", Author.Role.USER, current_date, "07411");
        authorDao.insert(admin);
        authorDao.insert(author1);
        authorDao.insert(author2);
//
        Project project1 = new Project(1, "Villa Mogliano", "04774", current_date, 2);
        Project project2 = new Project(2, "Potenza Picena", "04775", current_date, 3);
//        projectDao.insert(project1);
//        projectDao.insert(project2);
//
//        ArrayList<String> imageUris = new ArrayList<String>();
//        imageUris.add("content://com.android.providers.media.documents/document/image%3A92");

//        imageUris.add("file:///storage/emulated/0/Android/data/com.example.posfoto/files/Pictures/POSFOTO/photo.jpg,");
////
//      annotationDao.insert(new Annotation(1,
//                author1,
//                project1,
//                imageUris,
//                "Duck is Quack",
//                1,
//                1,
//                current_date));
//        annotationDao.insert(new Annotation(2,
//                author2,
//                project2,
//                imageUris,
//                "kapot raam op het eerste verdiep doordat er een éénd is doorgevlogen.",
//                2,
//                1,
//                current_date));
//
//        annotationDao.insert(new Annotation(3,
//                author2,
//                project2,
//                imageUris,
//                "kapot raam op het eerste verdiep doordat er een éénd is doorgevlogen.",
//                2,
//                1,
//                current_date));
//
//        annotationDao.insert(new Annotation(4,
//                author2,
//                project2,
//                imageUris,
//                "kapot raam op het eerste verdiep doordat er een éénd is doorgevlogen.",
//                2,
//                1,
//                current_date));


        return null;
    }
}
