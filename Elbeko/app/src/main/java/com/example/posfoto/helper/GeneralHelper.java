package com.example.posfoto.helper;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class GeneralHelper {

        public static void hideKeyboard(Activity activity) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }


    //    private void showDialog(ArrayList<Project> projects, Project current_project) {
//        FragmentManager fm = getSupportFragmentManager();
//        ProjectSelectDialogFragment projectSelectDialogFragment = ProjectSelectDialogFragment.newInstance(getResources().getString(R.string.elect_project_dialog_title), projects, current_project);
//        projectSelectDialogFragment.ProjectSelectDialogListener(MainActivity.this);
//        projectSelectDialogFragment.show(fm, "fragment_edit_name");
//          }



}
