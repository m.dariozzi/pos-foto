package com.example.posfoto.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.posfoto.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ImageGridAdapter extends RecyclerView.Adapter<ImageGridAdapter.ViewHolder>{


    private Context context;


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;
        FloatingActionButton deleteBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            deleteBtn = itemView.findViewById(R.id.deleteImageFAB);
        }
    }

    private List<Bitmap> mImageList;
    private final ImageGridAdapter.OnItemClickListener listener;

    public ImageGridAdapter(List<Bitmap> imageList, OnItemClickListener listener) {
        this.mImageList = imageList;
        this.listener = listener;
    }



    @NonNull
    @Override
    public ImageGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.image_grid_item, parent, false);


        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageGridAdapter.ViewHolder holder, int position) {
        if (!mImageList.isEmpty()) {
            Bitmap bmp_Copy = mImageList.get(position).copy(Bitmap.Config.ARGB_8888,true);
            Glide.with(context)
                    .load(bmp_Copy)
                    .placeholder(R.drawable.ic_photo_library)
                    .fitCenter()
                    .into(holder.iv);

            holder.itemView.setOnClickListener(view -> {
                listener.onItemClick(mImageList.get(position), position, view);
            });

            holder.itemView.setOnLongClickListener(view -> {
                listener.onItemLongClick(mImageList.get(position), view);
                return true;
            });

            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeItem(position);
                }
            });
        }
    }

    public void setImages(List<Bitmap> images) {
        this.mImageList = images;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mImageList.remove(position);
        notifyDataSetChanged();
    }

    public void clearAll() {
        mImageList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mImageList!= null) {
            return mImageList.size();
        } else {
            return 0;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Bitmap item, int position, View v);
        boolean onItemLongClick(Bitmap item, View v);
    }



}
