package com.example.posfoto.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.posfoto.R;
import com.example.posfoto.models.Project;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.ProjectViewHolder> {

    private Context context;

    class ProjectViewHolder extends RecyclerView.ViewHolder{

//        private final ImageView projectImageView;
        private final TextView projectItemTitle;
        private final TextView projectItemText1;
        private final TextView projectItemText2;
        private final TextView projectItemTextDate;
//        private final Button projectItemSendBtn;

        public ProjectViewHolder(@NonNull View itemView) {
            super(itemView);
//            projectImageView = itemView.findViewById(R.id.list_item_picture);
            projectItemTitle = itemView.findViewById(R.id.list_item_title);
            projectItemText1 = itemView.findViewById(R.id.list_item_text1);
            projectItemText2 = itemView.findViewById(R.id.list_item_text2);
            projectItemTextDate = itemView.findViewById(R.id.list_item_date);
//            projectItemSendBtn = itemView.findViewById(R.id.btnSendSingleAnnotation);
        }

        public void bind(final Project item, final ProjectListAdapter.OnItemClickListener listener) {
//            Glide.with(context).load(R.drawable.ic_projects_white).into(projectImageView);
//            projectImageView.setColorFilter(Color.parseColor("#717171"));
            projectItemTitle.setText(item.getProjectName());

            projectItemTextDate.setText(item.getFormattedUpdated_at().toString());
//            projectItemText1.setText(item.ge);
//            projectItemText2.setText(item.());
//            projectItemSendBtn.setVisibility(View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, v);
                }
            });

            itemView.setOnLongClickListener(v -> {
                listener.onItemLongClick(item, v);

                return true;
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Project item, View v);
        boolean onItemLongClick(Project item, View v);
    }


    private final LayoutInflater inflater;

    private List<Project> projects;

    private final ProjectListAdapter.OnItemClickListener listener;

    public ProjectListAdapter(Context context, ProjectListAdapter.OnItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.list_item, parent, false);
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectViewHolder holder, int position) {
        if (projects != null) {
            holder.bind(projects.get(position), listener);

        } else {
            holder.projectItemTitle.setText("no projects");
        }
    }


    public void setProjects(List<Project> projects){
        this.projects = projects;
        notifyDataSetChanged();

    }

    public List<Project> getProjects() {
        return this.projects;
    }

    @Override
    public int getItemCount() {
        if (projects!= null) {
            return projects.size();
        } else {
            return 0;
        }
    }

}
