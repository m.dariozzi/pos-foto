package com.example.posfoto.network;

import com.example.posfoto.models.Bearertoken;
import com.example.posfoto.models.Logindata;
import com.example.posfoto.network.APIs.AuthorService;
import com.example.posfoto.sessions.SessionManager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;

public class TokenAuthenticator implements Authenticator {

    SessionManager session;

    public TokenAuthenticator(SessionManager session) {
        this.session = session;
    }

    @Nullable
    @Override
    public Request authenticate(@Nullable Route route, @NotNull Response response) throws IOException {
        AuthorService service = ServiceGenerator.createService(AuthorService.class);
        Logindata data = new Logindata();
        data.username = session.getUserAsAuthor().getUsername();
        data.password = session.getUserAsAuthor().getPassword();

        Call<Bearertoken> request = service.getToken(data);
        Bearertoken token = request.execute().body();

        session.setBearerToken(token);

        return response.request().newBuilder().header("Authorization", "Bearer " + token).build();
    }
}
