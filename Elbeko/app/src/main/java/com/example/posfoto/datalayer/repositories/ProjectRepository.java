package com.example.posfoto.datalayer.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.example.posfoto.datalayer.AnnotationRoomDatabase;
import com.example.posfoto.models.DAOs.ProjectDao;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.ProjectwithSubprojects;

import java.util.List;

import androidx.lifecycle.LiveData;

public class ProjectRepository {
    private ProjectDao projectDao;

    private LiveData<List<Project>> allProjects;

    public ProjectRepository(Application application) {
        AnnotationRoomDatabase db = AnnotationRoomDatabase.getDatabase(application);

        projectDao = db.projectDao();
        allProjects = projectDao.getAllProjects();
    }

    public LiveData<List<Project>> getAllProjects() {
        return allProjects;
    }

    public LiveData<List<Project>> getProjectById(int id) { return projectDao.getProjectById(id);}

    public void insert(Project project) {
        new insertAsyncTask(projectDao).execute(project);
    }

    public void deleteById(int id) {  new ProjectRepository.deleteAsyncTask(projectDao).execute(id); }

    public void update(Project project) { new updateAsyncTask(projectDao).execute(project); }

    public LiveData<List<Project>> getOldProject(List<Integer> listIdProject) {
        return projectDao.getOldProjects(listIdProject);
    }


    public LiveData<List<ProjectwithSubprojects>> getProjectsWithSubprojects() {
        return projectDao.getProjectsWithSubProjects();
    }

    public void clearTable() {
       new deleteAllAsyncTask(projectDao, result -> {
           Log.d("TAG", "TaskCompletionResult: ok" + result.toString());
           return result;
       }).execute();

    }

    public int clearTableCompletable() {
        return projectDao.deleteAllCompletable();
    }

    public void deleteOldProj(List<Integer> lstIdProject) {
        new deleteOldAsyncTask(projectDao).execute(lstIdProject);
    }


    private static class deleteOldAsyncTask extends AsyncTask<List<Integer>, Void, Void> {
        private ProjectDao AsyncTaskDao;

        deleteOldAsyncTask(ProjectDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(List<Integer>... params) {
            AsyncTaskDao.deleteOldProj(params[0]);
            return null;
        }
    }

    /**
     * ASYNC tasks
     */

    public interface TaskDelegate {
        public Integer TaskCompletionResult(Integer result);
    }


    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Integer> {
        private ProjectDao AsyncTaskDao;
        private TaskDelegate delegate;

        deleteAllAsyncTask(ProjectDao dao) {
            AsyncTaskDao = dao;
        }

        deleteAllAsyncTask(ProjectDao dao, TaskDelegate delegate) {
            AsyncTaskDao = dao;
            this.delegate = delegate;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return AsyncTaskDao.deleteAll();


        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            delegate.TaskCompletionResult(integer);
        }
    }

    private static class updateAsyncTask extends AsyncTask<Project, Void, Void> {
        private ProjectDao AsyncTaskDao;

        updateAsyncTask(ProjectDao dao) { AsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(Project... params) {
            AsyncTaskDao.update(params[0]);
            return null;
        }
    }


    private static class insertAsyncTask extends AsyncTask<Project, Void, Void> {

        private ProjectDao AsyncTaskDao;
        boolean success;

        insertAsyncTask(ProjectDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Project... params) {
            try {
                AsyncTaskDao.insert(params[0]);
            } catch (Exception e) {

                e.printStackTrace();
                Log.d("TAG", "exception thrown");
                success = false;
                cancel(false);
            }
            success = true;
            return null;
        }


        @Override
        protected void onPostExecute(Void avoid) {
            super.onPostExecute(avoid);
            if (success) {

            } else {

            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            //error post processing here -> do something
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private ProjectDao AsyncTaskDao;

        deleteAsyncTask(ProjectDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            AsyncTaskDao.deleteById(params[0]);
            return null;
        }
    }
}
