package com.example.posfoto.fragments.dialog;

import android.app.Dialog;
import android.os.Bundle;

import com.example.posfoto.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServerSelectDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServerSelectDialogFragment extends DialogFragment {

    private static String SERVERS_KEY;
    private String[] servers;
    private String selected_server;
    private String Title;
    int selected;

    public ServerSelectDialogFragment() {
        // Required empty public constructor
    }

    ServerSelectDialogInterface list;

    public void ServerSelectDialogListener(ServerSelectDialogInterface list) { this.list = list; }

    public static ServerSelectDialogFragment newInstance(String Title, String[] servers, String selected_server) {
        ServerSelectDialogFragment fragment = new ServerSelectDialogFragment();
        Bundle args = new Bundle();
        args.putString("Title", Title);
        args.putStringArray(SERVERS_KEY, servers);
        args.putString("selected_server", selected_server);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.Title = getArguments().getString("title");
            this.servers = getArguments().getStringArray(SERVERS_KEY);
            this.selected_server = getArguments().getString("selected_server");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
        builder.setNeutralButton(getResources().getString(R.string.cancel), (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });

        builder.setPositiveButton(getResources().getString(R.string.ok), (dialogInterface, i) -> {
            list.onFinishedSelect(selected);
        });

        ArrayList<String> test = new ArrayList<>();
        for (int i = 0; i < servers.length; i++) {
            test.add(servers[i]);
        }

        int index = test.indexOf(selected_server);

        builder.setSingleChoiceItems(servers, index, (dialogInterface, i) -> {
            selected = i;
        });
        return builder.create();
    }

    /**
     * INTERFACE
     */

    public interface ServerSelectDialogInterface {
        void onFinishedSelect(int index);
    }
}