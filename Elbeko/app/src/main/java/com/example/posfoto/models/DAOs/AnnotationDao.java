package com.example.posfoto.models.DAOs;

import com.example.posfoto.models.Annotation;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface AnnotationDao {

    @Insert
    void insert(Annotation annotation);

    @Update
    void update(Annotation annotation);

    @Query("DELETE FROM annotations")
    void deleteAll();

    @Query("DELETE from annotations WHERE id = :id")
    void deleteById(int id);

    @Query("select annotations.*, authors.*, projects.* from annotations " +
            "inner join authors on annotations.author_id = authors.authorId " +
            "inner join projects on annotations.project_id = projects.projectId " +
            "WHERE annotations.author_id = :userId")
    LiveData<List<Annotation>> getAllAnnotationsById(int userId);

    @Query("select annotations.*, authors.*, projects.* from annotations inner join authors " +
            "on annotations.author_id = authors.authorId inner join projects " +
            "on annotations.project_id = projects.projectId")
    LiveData<List<Annotation>> getAllAnnotations();

    @Query("select * from annotations " +
            "inner join authors on annotations.author_id = authors.authorId" +
            " inner join projects on annotations.project_id = projects.projectId")
    List<Annotation> staticGetAllAnnotations();

    @Query("select annotations.* from annotations " +
            "where annotations.status = 0")
    LiveData<List<Annotation>> getUnsentAnnotations();

    @Query("SELECT * from annotations " +
            "INNER JOIN projects on annotations.project_id = projects.projectId" +
            " where annotations.status = 0 and project_id = :projectId")
    LiveData<List<Annotation>> getUnsentAnnotationsByProject(int projectId);

    @Query("SELECT annotations.*, authors.*, projects.* from annotations " +
            "inner join authors on annotations.author_id = authors.authorId " +
            "inner join projects on annotations.project_id = projects.projectId WHERE annotations.status = 2")
    LiveData<List<Annotation>> getSentAnnotations();
}

