package com.example.posfoto.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.posfoto.BuildConfig;
import com.example.posfoto.R;
import com.example.posfoto.adapters.ServerSelectAdapter;
import com.example.posfoto.application.MyAppBaseActivity;
import com.example.posfoto.datalayer.repositories.AuthorRepository;
import com.example.posfoto.datalayer.viewmodels.AnnotationViewModel;
import com.example.posfoto.datalayer.viewmodels.AuthorViewModel;
import com.example.posfoto.datalayer.viewmodels.ProjectViewModel;
import com.example.posfoto.helper.GeneralHelper;
import com.example.posfoto.helper.GlobalVariables;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Bearertoken;
import com.example.posfoto.models.Logindata;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.User;
import com.example.posfoto.network.APIs.AuthorService;
import com.example.posfoto.network.APIs.ProjectService;
import com.example.posfoto.network.RetrofitClientInstance;
import com.example.posfoto.sessions.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LoginActivity extends MyAppBaseActivity  {

    EditText txtUsername;
    EditText txtPassword;
    Button btnLogin;

    ServerSelectAdapter adapter;
    Spinner spnSelectLoginServer;

    SessionManager session;

    ProgressBar loadingBar;

    private AuthorRepository repo;
    private AuthorViewModel AuthorViewModel;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        repo = new AuthorRepository(getApplication());
        session = new SessionManager(getApplicationContext());

        checkFirstRun();

        txtUsername =  findViewById(R.id.txtUsername);
        txtPassword =  findViewById(R.id.txtPassword);

//        txtUsername.setText("VE1");
//        txtPassword.setText("6825");

        btnLogin = findViewById(R.id.btnLogin);

        adapter = new ServerSelectAdapter(this, R.layout.support_simple_spinner_dropdown_item);
        spnSelectLoginServer = findViewById(R.id.spnSelectLoginServer);
        spnSelectLoginServer.setAdapter(adapter);

        List<String> urls = new ArrayList<>(Arrays.asList(BuildConfig.URL_ARRAY));
        adapter.setUrls(urls);

        AuthorViewModel = new ViewModelProvider(this).get(AuthorViewModel.class);

        loadingBar = findViewById(R.id.progress_loader);
        loadingBar.setVisibility(View.GONE);

        Intent i = new Intent(getApplicationContext(), MainActivity.class);

        if (session.isLoggedIn()) { startActivity(i); finish(); }

        btnLogin.setOnClickListener(view -> {
            String username = txtUsername.getText().toString();
            String password = txtPassword.getText().toString();

            Logindata data = new Logindata();
            data.username = username;
            data.password = password;


            loadingBar.setVisibility(View.VISIBLE);


            String BASE_URL = adapter.getServerUrls().get(spnSelectLoginServer.getSelectedItemPosition());

            Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance(getApplicationContext());
            RetrofitClientInstance.getHostInterceptor().setHost(BASE_URL);
            AuthorService api = retrofit.create(AuthorService.class);


            GlobalVariables.isSuperUser = false;

            if (data.username.equals("Super") && data.password.equals("Super")) {
                //login as super user
                loadingBar.setVisibility(View.GONE);
                GeneralHelper.hideKeyboard(LoginActivity.this);
                Toast.makeText(LoginActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                session.createLoginSession(String.valueOf(666), "super@user.ecs-website.com", "Super", "Super", Author.Role.MASTER.toString(), "", String.valueOf(666));
                btnLogin.getBackground().setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.colorGreen), PorterDuff.Mode.MULTIPLY);

                loadingBar.setVisibility(View.GONE);

                //fill the
                GlobalVariables.isSuperUser = true;
                GeneralHelper.hideKeyboard(LoginActivity.this);
                Intent in = new Intent(getApplicationContext(), InitActivity.class);
                startActivity(in);
                finish();
            }

            if (GlobalVariables.isNetworkConnected) {

                api.login(data).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        loadingBar.setVisibility(View.GONE);
                        GeneralHelper.hideKeyboard(LoginActivity.this);

                        if (response.isSuccessful()) {
                            user = response.body();
                            api.getToken(data).enqueue(new Callback<Bearertoken>() {
                                @Override
                                public void onResponse(Call<Bearertoken> call, Response<Bearertoken> response) {
                                    Toast.makeText(LoginActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                                    Bearertoken token = response.body();
                                    session.createLoginSession(String.valueOf(user.getAuthorId()), user.getEmail(), user.getUsername(), data.password, user.getRole(), token.getAccessToken(), user.getPOS_internal_code());
                                    //add a refresh token to the sessionstorage or android key store
                                    //expi

                                    btnLogin.getBackground().setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.colorGreen), PorterDuff.Mode.MULTIPLY);

                                    //get all data from the masters.
                                    //en vul de database.

                                    Sync(retrofit);

                                }

                                @Override
                                public void onFailure(Call<Bearertoken> call, Throwable t) {
                                    Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                                    t.printStackTrace();
                                    showLoginerrorMessage();
                                }
                            });

                        } else {
                            //not logged in
                            showLoginerrorMessage();

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        loadingBar.setVisibility(View.GONE);
                    }
                });

            } else {
//
                AuthorViewModel autoAuthorViewModel = new ViewModelProvider(this).get(AuthorViewModel.class);
                autoAuthorViewModel.getAuthorByLoginData(data.username, data.password).observe(this, new Observer<Author>() {
                    @Override
                    public void onChanged(Author author) {
                        if (author != null) {
                            session.createLoginSession(String.valueOf(author.getAuthorId()), author.getEmail(), data.password ,author.getUsername(),
                                    author.getRole().toString(), null, author.getPOS_internal_code());


                            /**
                             * UI manipulatie
                             */
                            btnLogin.setEnabled(false);
                            txtUsername.setEnabled(false);
                            txtPassword.setEnabled(false);
                            btnLogin.getBackground().setColorFilter(ContextCompat.getColor(LoginActivity.this, R.color.colorGreen), PorterDuff.Mode.MULTIPLY);

                            loadingBar.setVisibility(View.GONE);

                            //fill the
                            GeneralHelper.hideKeyboard(LoginActivity.this);


                            Intent i = new Intent(getApplicationContext(), InitActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            showLoginerrorMessage();
                        }


                    }
                });
            }
        });
    }

    private void Sync(Retrofit retrofit)  {
        ProjectService api2 = retrofit.create(ProjectService.class);
//        api2.getProjectByAuthor(String.valueOf(session.getUserAsAuthor().getAuthorId())).enqueue(new Callback<List<Project>>() {
        api2.getAllProjects().enqueue(new Callback<List<Project>>() {
            @Override
            public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {
                //show spinner met projecten in.

                List<Integer> lstIdProject = new ArrayList<>();

                if (response.code() == 200) {
                    ProjectViewModel projectViewModel = new ViewModelProvider(LoginActivity.this).get(ProjectViewModel.class);

                    for (Project project : response.body()) {
                        Project newProject = new Project(project.getProjectId(), project.getProjectName(), project.getPOSInternalCode(),
                                project.getUpdated_at(), project.getProjectManagerId());
                        projectViewModel.insert(newProject);
                        lstIdProject.add(project.getProjectId());
                        Log.d("TAG", "inserted");
                    }


                    projectViewModel.deleteOldProjects(lstIdProject);

                    /**
                     * UI manipulatie
                     */

                }
            }

            @Override
            public void onFailure(Call<List<Project>> call, Throwable t) {
                t.printStackTrace();
            }
        });


        AuthorService api = retrofit.create(AuthorService.class);
        api.getAllAuthors().enqueue(new Callback<List<Author>>() {
            @Override
            public void onResponse(Call<List<Author>> call, Response<List<Author>> response) {

                List<Integer> lstIdAuthor = new ArrayList<>();
                if (response.code() == 200) {
                    AuthorViewModel authorViewModel = new ViewModelProvider(LoginActivity.this).get(AuthorViewModel.class);

                    for (Author author : response.body()) {

                        Author newAuthor = new Author(author.getAuthorId(), author.getFirstName(), author.getLastName(), author.getEmail(),
                                author.getUsername(), author.getPassword(), Author.Role.USER, author.getUpdated_at(), author.getPOS_internal_code());
                        authorViewModel.insert(newAuthor);
                        lstIdAuthor.add(author.getAuthorId());
                    }
                    authorViewModel.deleteOldAuthors(lstIdAuthor);

                    Intent i = new Intent(getApplicationContext(), InitActivity.class);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<List<Author>> call, Throwable t) {
                t.printStackTrace();
                //een oplossing vinden voor de errors
            }
        });



    }

    private void showLoginerrorMessage() {
        loadingBar.setVisibility(View.GONE);
        findViewById(R.id.txtLoginError).setVisibility(View.VISIBLE);
        findViewById(R.id.txtLoginError).postDelayed(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.txtLoginError).setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    private void checkFirstRun() {
        final String PREFS_NAME = "MyPrefsFile";
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;

        AnnotationViewModel annotationViewModel = new ViewModelProvider(this).get(AnnotationViewModel.class);
        ProjectViewModel projectViewModel = new ViewModelProvider(this).get(ProjectViewModel.class);
        AuthorViewModel authorViewModel = new ViewModelProvider(this).get(AuthorViewModel.class);

        int currentVersionCode = BuildConfig.VERSION_CODE;
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        if (currentVersionCode == savedVersionCode) {
            //normal run
            Log.d("TAG", "normal");
            Log.d("TAG", session.getSelectedServer());
            return;
        } else if (savedVersionCode == DOESNT_EXIST) {
            //new run
            Log.d("TAG", "new");
            Log.d("TAG", session.getSelectedServer());
            return;


        } else if (currentVersionCode > savedVersionCode) {
            //this is an upgrade
            Log.d("TAG", "upgrade");
            return;
        }

        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}
