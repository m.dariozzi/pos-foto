package com.example.posfoto.models.TypeConverters;

import com.example.posfoto.models.Annotation;

import androidx.room.TypeConverter;
import com.example.posfoto.models.Annotation.Status;

public class StatusTypeConverter {

    @TypeConverter
    public static Annotation.Status toStatus(int status) {
        if (status == Status.NOT_SENT.getCode()) {
            return Status.NOT_SENT;
        } else if (status == Status.SENDING.getCode()) {
            return Status.SENDING;
        } else if (status == Status.SENT.getCode()) {
            return Status.SENT;
        } else {
            throw new IllegalArgumentException("Could not recognize status");
        }
    }

    @TypeConverter
    public static int toInteger(Annotation.Status status) {
        return status.getCode();
    }
}