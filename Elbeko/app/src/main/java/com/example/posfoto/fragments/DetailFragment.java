package com.example.posfoto.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.posfoto.R;
import com.example.posfoto.activities.FullImageActivity;
import com.example.posfoto.models.Annotation;
import com.example.posfoto.sessions.SessionManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class DetailFragment extends Fragment {

    View view;
    private static final String ANNOTATION_KEY = "annotation_key";
    private static final String DATA_SOURCE = "data_source";
    private String BASE_URL;
    SessionManager session;


    private Annotation annotation;
    private int mdata_source;


    public static final String ARG_ANNOTATION = "ARG_ANNOTATION";

    public DetailFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(Annotation annotation, int dataSource) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ANNOTATION_KEY, annotation);
        args.putInt(DATA_SOURCE, dataSource);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getContext());
        BASE_URL = session.getSelectedServer();
        if (getArguments() != null) {
            this.annotation = (Annotation) getArguments().getSerializable(ANNOTATION_KEY);
            this.mdata_source = (int) getArguments().getInt(DATA_SOURCE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);
        annotation = (Annotation) getArguments().getSerializable(ANNOTATION_KEY);

        setHasOptionsMenu(true);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.image_container);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 20, 20, 20);
        layoutParams.gravity = Gravity.CENTER;

        TextView txtTitle = getView().findViewById(R.id.txtDetailTitle);
        TextView txtText = getView().findViewById(R.id.txtDetailText);
        TextView txtAuthor = getView().findViewById(R.id.txtDetailAuthor);
        TextView txtProject = getView().findViewById(R.id.txtDetailProject);
        TextView txtStatus = getView().findViewById(R.id.txtDetailStatus);
        TextView txtDate = getView().findViewById(R.id.txtDetailDate);
        TextView noImagesTxt = getView().findViewById(R.id.noImagesTxt);

        ProgressBar loading = getView().findViewById(R.id.progress_loader);

        if (!annotation.getPictures().isEmpty()) {
            noImagesTxt.setVisibility(View.GONE);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round);

            for (String url :
                    annotation.getPictures()) {
                ImageView view = new ImageView(getActivity());
                String imageUrl;

                if (url.contains("content://media/external")) {
                    imageUrl = url;
                } else {
                    imageUrl = BASE_URL + "/pos-foto-api/annotations/getImageByName/" + url;
                }


                Glide.with(this).load(imageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        loading.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        loading.setVisibility(View.GONE);
                        return false;
                    }
                }).apply(options).into(view);

                view.setLayoutParams(layoutParams);
                layout.addView(view);

                view.setOnClickListener(view1 -> {
                    view.buildDrawingCache();
                    Bitmap bitmap = view.getDrawingCache();

                    Intent intent = new Intent(getContext(), FullImageActivity.class);
                    intent.putExtra("BitmapImage", bitmap);
                    startActivity(intent);


                });
            }
        } else {

            noImagesTxt = view.findViewById(R.id.noImagesTxt);
            noImagesTxt.setVisibility(View.VISIBLE);
        }


        txtTitle.setText("Annotation NR: " + annotation.getId());
        txtAuthor.setText(String.valueOf(annotation.getAuthorId()));
        txtProject.setText(annotation.getProject().getProjectName());
        txtAuthor.setText(annotation.getAuthor().getUsername());


        if (mdata_source == 1) {
            txtStatus.setText("Status: Lokaal");
            txtStatus.setTextColor(Color.GREEN);
        } else if (mdata_source == 2) {
            txtStatus.setText("Status: op server");
            txtStatus.setTextColor(Color.RED);
        }

        txtDate.setText(annotation.getFormattedCreated_at());
        txtText.setText(annotation.getText());

    }


}