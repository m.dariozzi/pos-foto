package com.example.posfoto.datalayer.repositories;

import android.app.Application;
import android.os.AsyncTask;

import com.example.posfoto.datalayer.AnnotationRoomDatabase;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.DAOs.AuthorDao;
import com.example.posfoto.network.APIs.AuthorService;
import com.example.posfoto.network.RetrofitClientInstance;

import java.util.List;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Retrofit;

public class AuthorRepository
{
    private AuthorDao authorDao;

    Retrofit retrofit;

    private LiveData<List<Author>> allAuthors;

    public AuthorRepository(Application application) {
        AnnotationRoomDatabase db = AnnotationRoomDatabase.getDatabase(application);
        retrofit = RetrofitClientInstance.getRetrofitInstance(application);

        authorDao = db.authorDao();
        allAuthors = authorDao.getAllAuthors();
    }

    public LiveData<List<Author>> getAllAuthors() { return allAuthors; }

    public void insert(Author author) { new insertAsyncTask(authorDao).execute(author); }

    public LiveData<Author> getAuthorByLoginData(String username, String password) { return authorDao.getAuthorByLoginData(username, password); }

    public void deleteById(int id) {  new AuthorRepository.deleteAsyncTask(authorDao).execute(id); }

    public void update(Author author) { new updateAsyncTask(authorDao).execute(author); }

    public LiveData<List<Author>> getAllAuthorsById(int id) { return authorDao.GetAllAuthorsById(id); }

    public List<Author> staticGetAllAuthors() { return authorDao.staticGetAllAuthors(); }

    public void clearTable() { new AuthorRepository.deleteAllAsyncTask(authorDao).execute(); }

    public void deleteOldAuthors(List<Integer> lstIdAuthor) {
        new deleteOldAsyncTask(authorDao).execute(lstIdAuthor);
    }

    private static class updateAsyncTask extends AsyncTask<Author, Void, Void> {
        private AuthorDao AsyncTaskDao;

        updateAsyncTask(AuthorDao dao) { AsyncTaskDao = dao; }

        @Override
        protected Void doInBackground(Author... params) {
            AsyncTaskDao.update(params[0]);
            return null;
        }
    }

    private static class insertAsyncTask extends AsyncTask<Author, Void, Void> {

        private AuthorDao AsyncTaskDao;

        insertAsyncTask(AuthorDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Author... params) {
            AsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private AuthorDao AsyncTaskDao;

        deleteAsyncTask(AuthorDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            AsyncTaskDao.deleteById(params[0]);
            return null;
        }
    }

    private static class deleteOldAsyncTask extends AsyncTask<List<Integer>, Void, Void> {
        private AuthorDao AsyncTaskDao;

        deleteOldAsyncTask(AuthorDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(List<Integer>... params) {
            AsyncTaskDao.deleteOld(params[0]);
            return null;
        }
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private AuthorDao AsyncTaskDao;

        deleteAllAsyncTask(AuthorDao dao) {
            AsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            AsyncTaskDao.deleteAll();
            return null;

        }
    }



    public Call<List<Author>> getAuthorsFromServer() {
        AuthorService api = retrofit.create(AuthorService.class);
        return api.getAllAuthors();
    }

//    public Call<Author> login(Logindata data) {
//        AuthorsAPI api = retrofit.create(AuthorsAPI.class);
//        return api.login(data);
//
//    }




}
