package com.example.posfoto.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.posfoto.R;

import androidx.appcompat.app.AppCompatActivity;

public class FullImageActivity extends AppCompatActivity {

    ImageView imgFullImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        imgFullImage = findViewById(R.id.fullscreenImageView);

        Bitmap bitmap = (Bitmap) getIntent().getParcelableExtra("BitmapImage");

        imgFullImage.setImageBitmap(bitmap);



    }
}