package com.example.posfoto.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.util.Log;
import android.view.View;

import com.example.posfoto.network.interfaces.CheckNetworkInterface;
import com.example.posfoto.sessions.SessionManager;

import androidx.annotation.NonNull;

public class CheckNetwork {
    private Context context;
    private SessionManager session;

    CheckNetworkInterface mEvents;


    public CheckNetwork(Context context, SessionManager session, CheckNetworkInterface mEvents) {
        this.context = context;
        this.session = session;
        this.mEvents = mEvents;

    }

    public void registerNetworkCallback(View currentView) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkRequest.Builder builder = new NetworkRequest.Builder();

            connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                   @Override
                   public void onAvailable(@NonNull Network network) {
                       GlobalVariables.isNetworkConnected = true;
                        mEvents.onAvailable();
                   }

                   @Override
                   public void onLost(@NonNull Network network) {
                       Log.d("TAG", "onLost: is not connected");
                       //if not connected, change toolbar
                       GlobalVariables.isNetworkConnected = false;
                       mEvents.onLost();
                   }
               }

            );
            GlobalVariables.isNetworkConnected = false;
        } catch (Exception e) {
            GlobalVariables.isNetworkConnected = false;
        }
    }
}


