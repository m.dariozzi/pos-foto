package com.example.posfoto.models;

import javax.annotation.Nonnull;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "subprojects")
public class Subproject {

    @PrimaryKey(autoGenerate = true)
    @Nonnull
    private int id;

    private String name;

    private int pos_internal_code;

    private int linked_project_id;


    public Subproject(int id, String name, int pos_internal_code, int linked_project_id) {
        this.id = id;
        this.name = name;
        this.pos_internal_code = pos_internal_code;
        this.linked_project_id = linked_project_id;
    }

    public int getLinked_project_id() {
        return linked_project_id;
    }

    public void setLinked_project_id(int linked_project_id) {
        this.linked_project_id = linked_project_id;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPos_internal_code() {
        return pos_internal_code;
    }

    public void setPos_internal_code(int pos_internal_code) {
        this.pos_internal_code = pos_internal_code;
    }
}
