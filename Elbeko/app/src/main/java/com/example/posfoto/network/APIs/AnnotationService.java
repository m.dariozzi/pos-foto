package com.example.posfoto.network.APIs;

import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Serverdata;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface AnnotationService {

    //get all annotations
    @GET("pos-foto-api/annotations")
    Call<List<Serverdata>> getAllAnnotations();

    //get Single annotation
    @GET("pos-foto-api/annotations/{id}")
    Call<Serverdata> getSingleAnnotation(@Path("id") String id);

    @GET("pos-foto-api/annotations/project/{project_id}")
    Call<List<Serverdata>> getAnnotationsForProject(@Path("project_id") String id);

    @GET("pos-foto-api/annotations/author/{author_id}")
    Call<Serverdata> getAnotationsForAuthor(@Path("author_id") String id);

    @POST("pos-foto-api/annotations/save")
    Call<String> save(@Body Annotation body);

//    @Multipart
//    @POST("pos-foto-api/annotations/save_picture")
//    Call<ResponseBody> save_picture(@Part MultipartBody.Part file,
//                                    @Part("annotation") Annotation annotation);
    @Multipart
    @POST("pos-foto-api/annotations/save_picture")
    Call<ResponseBody> save_picture(@Part List<MultipartBody.Part> file,
                                    @PartMap Map<String, Object> annotation);
}
