package com.example.posfoto.models.DAOs;

import com.example.posfoto.models.Project;
import com.example.posfoto.models.ProjectwithSubprojects;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

@Dao
public interface ProjectDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Project project) throws Exception;

    @Update
    void update(Project project);

    @Query("DELETE FROM projects")
    int deleteAll();

    @Query("SELECT * FROM projects WHERE projectId not IN(:lstIdProj)")
    LiveData<List<Project>> getOldProjects(List<Integer> lstIdProj);

    @Query("DELETE FROM projects WHERE projectId NOT IN(:lstIdProj)")
    void deleteOldProj(List<Integer> lstIdProj);

    @Query("DELETE FROM projects")
    int deleteAllCompletable();

    @Query("DELETE FROM projects WHERE projectId= :id")
    void deleteById(int id);

    @Query("SELECT projects.*, authors.* from projects inner join authors on projects.project_manager_id = authors.authorId")
    LiveData<List<Project>> getAllProjects();

    @Query("SELECT projects.*, authors.* from projects inner join authors on projects.project_manager_id = authors.authorId")
    List<Project> staticGetAllProjects();

    @Query("SELECT * FROM projects where projectId = :id")
    LiveData<List<Project>> getProjectById(int id);

    @Transaction
    @Query("SELECT * FROM projects")
    public LiveData<List<ProjectwithSubprojects>> getProjectsWithSubProjects();
}
