package com.example.posfoto.models.TypeConverters;

import com.example.posfoto.models.Author;

import androidx.room.TypeConverter;


public class RoleTypeConverter {

    @TypeConverter
    public static Author.Role toRole(int role) {
        if (role == Author.Role.MASTER.getCode()) {
            return Author.Role.MASTER;
        } else if (role == Author.Role.USER.getCode()) {
            return Author.Role.USER;
        } else {
            throw new IllegalArgumentException("Could not get correct row");
        }
    }

    @TypeConverter
    public static int toInteger(Author.Role role) {
        return role.getCode();
    }


}
