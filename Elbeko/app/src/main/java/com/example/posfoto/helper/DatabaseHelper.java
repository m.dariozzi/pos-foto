package com.example.posfoto.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;

import com.example.posfoto.models.Annotation;
import com.example.posfoto.models.Author;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.Serverdata;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.core.content.FileProvider;

public class DatabaseHelper {

    //private static List<Author> m_authors = new ArrayList<>();
    //private static List<Project> m_projects = new ArrayList<>();

    private static Author m_author;
    private static Project m_project;

    public static List<Annotation> synchronizeDatabase(List<Serverdata> data, Context context, List<Author> authors, List<Project> projects) {
        /*
         @TODO alle synchronisatie operaties.

         1) annotation objecten van maken.
         2) De foto's aanmeken en linken aan een annotatie object
         3) de datums vergelijkenµ

         4) de database synchroniseren.
         */


        List<Annotation> annotations = new ArrayList<>();
        ArrayList<String> imageUriStrings = new ArrayList<>();

        byte[] decodedString;

        for (Serverdata serverdata : data) {
            for (int i = 0; i <= Integer.parseInt(serverdata.getNumberOfPictures()); i++) { }
            //getpicture het cijfer in de naam van de picture

            decodedString = Base64.decode(serverdata.getPictures().get(0), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            File photoFile = null;
            try {
                photoFile = PictureHelper.createImageFile(context);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(context, "com.example.android.FileProvider", photoFile);
                imageUriStrings.add(photoURI.toString());
            }


            m_author = authors.stream().filter(a -> a.getAuthorId() ==  Integer.parseInt(serverdata.getAuthorId())).findFirst().get();
            m_project = projects.stream().filter(p ->  p.getProjectId() == Integer.parseInt(serverdata.getProjectId())).findFirst().get();



            annotations.add(new Annotation(
                    m_author,
                    m_project,
                    imageUriStrings,
                    serverdata.getText(),
                    Integer.parseInt(serverdata.getAuthorId()),
                    Integer.parseInt(serverdata.getProjectId()),
                    new Date(serverdata.getCreatedAt())));
        }
        return annotations;
    }


}
