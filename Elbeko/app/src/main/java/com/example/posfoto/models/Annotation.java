package com.example.posfoto.models;

import com.example.posfoto.models.TypeConverters.AnnotationsTypeConverter;
import com.example.posfoto.models.TypeConverters.StatusTypeConverter;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;


@Entity(tableName = "annotations")
@TypeConverters({AnnotationsTypeConverter.class, StatusTypeConverter.class})
public class Annotation implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @Expose
    private int id;

    @Ignore
    private Author author;

    @Ignore
    private Project project;

    private ArrayList<String> pictures;

    private String text;

    @ColumnInfo(name = "author_id")
    public int authorId;

    @ColumnInfo(name = "project_id")
    public int projectId;

    public Status status;

    public Date created_at;

    public enum Status {
        NOT_SENT(0),
        SENDING(1),
        SENT(2);

        private int code;

        Status(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }


    @Ignore
    public Annotation(Author author, Project project, ArrayList<String> pictures, String text, int authorId, int projectId, Date created_at) {
        this.pictures = pictures;
        this.text = text;
        this.project = project;
        this.author = author;
        this.authorId = authorId;
        this.projectId = projectId;
        this.status = Status.NOT_SENT;
        this.created_at = created_at;
    }


    public Annotation(ArrayList<String> pictures, String text, int authorId, int projectId, Date created_at) {
        this.pictures = pictures;
        this.text = text;
        this.authorId = authorId;
        this.projectId = projectId;
        this.status = Status.NOT_SENT;
        this.created_at = created_at;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public String getStringId() {
        return "" + id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<String> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<String> pictures) {
        this.pictures = pictures;
    }

    public String getPictureByIndex(int index) { return pictures.get(index);}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public String getFormattedCreated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.getDefault());
        return dateFormat.format(created_at);
    }



    public void setCreated_at(Date created_at) { this.created_at = created_at; }

    public int getAuthorId() { return authorId; }

    public void setAuthorId(int authorId) { this.authorId = authorId; }

    public int getProjectId() { return projectId; }

    public void setProjectId(int projectId) { this.projectId = projectId; }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Annotation{" +
                "  id=" + id +
                ", author=" + authorId +
                ", project=" + projectId +
                ", text='" + text + '\'' +
                ", status'" + status +
                ", date'" + created_at +
                '}';
    }

}
