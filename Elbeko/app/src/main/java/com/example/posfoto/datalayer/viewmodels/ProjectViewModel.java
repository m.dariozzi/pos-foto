package com.example.posfoto.datalayer.viewmodels;

import android.app.Application;

import com.example.posfoto.datalayer.repositories.ProjectRepository;
import com.example.posfoto.models.Project;
import com.example.posfoto.models.ProjectwithSubprojects;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class ProjectViewModel extends AndroidViewModel {

    private ProjectRepository repository;
    private LiveData<List<Project>> allProjects;


    public ProjectViewModel(@NonNull Application application) {
        super(application);
        repository = new ProjectRepository(application);
    }

    public LiveData<List<Project>> getAllProjects() { return repository.getAllProjects(); }

    public LiveData<List<Project>> getProjectById(int id) { return repository.getProjectById(id); }

    public void deleteById(int id) { repository.deleteById(id); }

    public void insert(Project project) { repository.insert(project); }

    public void update(Project project) { repository.update(project); }

    public void clearTable() { repository.clearTable(); }

    public int clearTableCompletable() {
        return repository.clearTableCompletable();
    }

    public void deleteOldProjects(List<Integer> lstIdProject) {
        repository.deleteOldProj(lstIdProject);
    }

    public LiveData<List<Project>> getOldProjects(List<Integer> listIdProj) {
        return repository.getOldProject(listIdProj);
    }

    public LiveData<List<ProjectwithSubprojects>> getProjectWithSubprojects(){
        return repository.getProjectsWithSubprojects();
    }
}
