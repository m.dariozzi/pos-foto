const express = require('express');
const fs = require('fs');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const xmlparser = require('express-xml-bodyparser');
const xml2js = require('xml2js');
const base64Img = require('base64-img');

const app = express();
const jsonArray = [];

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}), xmlparser()); 


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => {
    res.send(req.url + 'received');
});

app.get('/api/annotations', (req, res) => {
    jsonArray.length = 0;
    console.log('request gotten');
    const files = readFilesFromDirectory('./annotations', '.xml');
    if (files instanceof Error) { res.status(400).send(files.message); }

    var promises = files.map(function(path) {
        return new Promise(function(path, resolve, reject) {
            fs.readFile(path, 'utf8', (err, data) => {
                if (err) { 
                    reject("error - paths not valid"); 
                } else {
                    xml2js.parseString(data, (err, result) => {
                        resolve(result);         
                    });
                }
            });
        }.bind(this, path));  
    });

    Promise.all(promises).then(results => {

        var final = results;
        for (let i = 0; i < results.length; i++) {
            jsonArray.push(results[i].root);
            
        }
        
        console.log(results.length)
        res.send(jsonArray);
    });
});


app.get('/api/projects', (req, res) => {

});

app.get('/api/users', (req, res) => {

});

app.post('/save_data', (req, res) => {
    if (!req.body) {
        res.status(400).send('Error, missing body');
    }

    if (!fs.existsSync('./annotations')) {
        fs.mkdir('./annotations', {recursive: true} , (err) => {
            if(err) throw err;
        });
    }

    let date = new Date(req.body.date);
    
    for (let i = 0; i < req.body.numberOfPictures; i++) {
        let savePicPath = 'Foto - id= ' + req.body.id + ', ' + i + 'of' + req.body.numberOfPictures + ', ' + date.getDate() + '-' + date.getMonth()+ '-' + date.getFullYear();
        if (req.body['picture' + i.toString()]) {   
            base64Img.img(req.body['picture' + i.toString()] , 'annotations', savePicPath, function(err, filepath) {
                console.log(err);
                console.log(filepath);
            });
        }
    }   


    let saveXmlPath = path.join('annotations', 'ANN - ' + 'id= ' + req.body.id + ', ' + req.body.author_internal_code + ', ' + req.body.project_internal_code + ', '  + date.getDate() + '-' + date.getMonth()+ '-' + date.getFullYear() + '.xml');
    const builder = new xml2js.Builder();  
    fs.writeFile(saveXmlPath, builder.buildObject(req.body), function(err) {
        if (err) throw err;
        console.log('all saved');
        res.status(200).send('xml was saved successfully');
    });
});



function readFilesFromDirectory(filepath, extension) {
    if (!fs.existsSync(filepath)) {
        console.log('no dir', filepath);
        return new Error('There are no annotations saved on the server.');
    }

    var files = fs.readdirSync(filepath);
    var fileList = [];
    for (let i = 0; i < files.length; i++) {
        var filename = path.join(filepath, files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            fromDir(filepath, extension);
        } else if (filename.indexOf(extension) >= 0) {
            fileList[i] = filename;
            
        }
    }
    return fileList;
}



var server = http.createServer(app);
server.listen(9000);
console.log('server started at port 9000 at ip');